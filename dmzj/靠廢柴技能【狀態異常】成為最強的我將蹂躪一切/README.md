# novel


- title: ハズレ枠の【状態異常スキル】で最強になった俺がすべてを蹂躙するまで
- title_zh: 靠廢柴技能【狀態異常】成為最強的我將蹂躪一切
- author: 篠崎芳
- illust: ＫＷＫＭ
- source: https://ncode.syosetu.com/n1785ek/
- cover: https://images-fe.ssl-images-amazon.com/images/I/51MgC3C%2BA8L.jpg
- publisher: syosetu
- date: 2019-08-04T12:24:00+08:00
- status: 連載
- novel_status: 0x0300

## illusts


## publishers

- dmzj

## series

- name: 靠废柴技能【状态异常】成为最强的我将蹂躏一切

## preface


```
有「背景人物」之稱的高中生三森燈河，和同班同學一起被召喚到異世界。同學陸續獲得A級、S級的勇者身分，卻只有燈河一個人是最低等的E級。而且他取得的技能，是在這個世界被視為「廢柴」的【狀態異常】。燈河被女神薇希斯拋棄到生存機率零的遺跡後，儘管面臨絕望的地獄處境，依舊善用自身技能蹂躪著魔物。在暗不見光的沉淪漆黑之中，燈河的心中只有一股意念──

「等我活著回去──你就認命吧。」

原本一心扮演背景人物的E級勇者，一路攀上絕對最強地位的逆襲故事，就此開幕！

　空気モブとして生きてきた高校生――三森灯河。
　修学旅行中に灯河はクラスメイトたちと異世界へ召喚されてしまう。
　召喚した女神によると最高ランクのＳ級や優秀なＡ級の勇者がこれほど集まった召喚は珍しいという。

　しかし灯河は唯一の最低ランク――Ｅ級勇者だった。

　ステータスは他のクラスメイトと比べると圧倒的に低い。しかも灯河の固有スキルはその異世界で絶対的なハズレとされている【状態異常スキル】で……。

　これはかつて空気モブだった廃棄勇者が、絶対最強の悪魔へと駆け上がる逆襲譚。
```

## tags

- node-novel
- dmzj
- 异界
- 日本
- 穿越
- R15
- syosetu
- エルフ嫁
- クラス転移
- スライム
- チート
- テンプレ
- ハイエルフの姫騎士
- ハイファンタジー
- ハイファンタジー〔ファンタジー〕
- ハーレム
- ファンタジー
- 主人公最強
- 残酷な描写あり
- 無双
- 現代メシ
- 男主人公
- 異世界召喚
- 異世界転移
- 蠅王
- 覚醒
- 魔獣

# contribute

- 黄瀞瑶
- 音无
- 风
- kid
- 撸管娘
- 

# options

## dmzj

- novel_id: 2656

## downloadOptions

- noFilePadend: true
- filePrefixMode: 4
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n1785ek

## textlayout

- allow_lf2: false

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n1785ek&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n1785ek/)
- https://comic-gardo.com/episode/10834108156661738245
- 

