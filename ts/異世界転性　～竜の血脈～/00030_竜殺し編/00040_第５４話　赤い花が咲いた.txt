「喂，竟然想逃跑阿」

在床上，希茲娜老老實實的正座面對著莉雅。

「因為形跡可疑所以就下手抓人，這樣做沒錯吧？」

同樣在這個房間裡的還有正在發問的吸血鬼明日香。

「嗯，做的好。因為中午稍微發生了一些事情」

原本打算趁著夜間摸黑逃跑的希茲娜，被偶然遇到的明日香給抓住了。

「原來如此阿，不行呦，怎麼可以不好好遵守約定呢？」

聽了事情的來竜去脈後，明日香抱著胳膊一副很偉大的樣子。

「但、但是我們都是女的阿！」

先不論雙方相處得如何，同樣都是女人，女人間做這種事一般而言是很異常的。

但是這種常識對明日香不管用。
對於活了很長一段時光接近永生的吸血鬼來說，對於自己喜歡的人根本不會去在意性別。
這大概也跟吸血鬼這個種族的繁殖力很低有關係吧。

實際上，魔族領地裡面確實存在的BL跟GL，不過這也不全然是魔王的錯就是了。

「有何不可？我平常也都是吸女孩子阿。」
「欸？」

吸？吸什麼？

「而且如果是她的話⋯⋯」

明日香一改常態認真的打量著莉雅。
她身上並沒有好色的氣息，而是散發一股追求著美麗事物的感覺。

「嗯，如果是這孩子的話我願意跟她一起睡」
「是嗎，我的話⋯⋯也可以跟妳一起睡」

難以置信。

希茲娜的雙親是關係非常良好的夫妻。
希茲娜是三個子女中的長女。
對她而言男人、女人、小孩這三者組合在一起才是普通的家庭。

如果自己是男性的話，現在應該早已愛上莉雅了吧。又或是將她當成女神般崇拜著吧，畢竟再也找不到像她這樣美麗的少女。
如果莉雅是男人的話⋯⋯
自己大概，不，絶對會⋯⋯

但這都只是假設而已。

───

「⋯⋯就這樣，有關科爾多巴的情報就這些。那麼，給我獎勵吧」
「沒辦法，來吧」

莉雅脫去上衣，那雪白的肌膚吸引住明日香的目光，而這一切都在希茲娜的面前上演著。

「嘿嘿，我開動囉」

就在希茲娜的面前，
明日香吻著莉雅的脖子。

希茲娜不懂自身在這一瞬間感受到的情感是什麼。
但這個情感很像憤怒。

「呼，我吃飽了」
「現、現在是⋯⋯」
「誒哆，親吻？」

因為暫時不能向她透露自己是吸血鬼這件事，所以呼攏了過去。
不過不愧是技巧高超的吸血鬼，不但看不到傷口也沒流血，只剩下紅色一片類似吻痕的東西。

「那麼，接下來」

莉雅放鬆了心情後，牽起了希茲娜的手。
手上傳來的力量很強，雖然她不是男的但這不是重點。
接著就這樣將希茲娜推倒在床上。

「我們講好了呦」
「欸、但是⋯」

抵抗的聲音非常微弱，面對如此強烈的要求，自己完全不知道該如何回應。

「妳也是戰士的話，就做好覺悟吧」
「這⋯⋯」

還來不及說出抗拒的話，雙唇就被堵住了。
連續而短暫的接吻，由於間隔的時間很短，讓希茲娜幾乎喘不過氣來。

「喔喔～是技巧派的阿」

明日香在一旁看著，雙頰變的紅潤，臉上帶著賊笑。

「這，妳這傢伙怎麼還在這裡阿？」
「欸？不能欣賞嗎？看這孩子的反應，大概是第一次所以特別可愛阿」
「不、不行⋯⋯」

希茲娜的聲音雖然微弱但仍清楚的說著。

「至少⋯⋯第一次⋯⋯⋯兩個人⋯⋯」
「阿，說的也是，那妳這個礙眼的傢伙閃邊涼快去」

明日香離開了專為莉雅準備的小屋，展開了『惡魔之翼』朝空中飛去，不過完全沒有人注意到。

───

「那麼，讓我們繼續吧」

莉雅一邊深情的接吻，一邊用手將希茲娜的衣服一件件脫了下來。

「這種方式的接吻，要把舌頭伸出來呦」

聽著莉雅的說明將舌頭伸了過去，結果被輕輕的咬了，那帶著甜蜜的疼痛甚至傳到了腰部。
眼眶不由得充滿著淚水。
莉雅的動作停了下來。

「妳、妳呀⋯」

腦袋明明還在思考著，但話卻已經問出口了。

「關於我的事，其實妳並不是真的喜歡我對吧？」

聞言，讓莉雅不禁想生氣。
雖然她很喜歡欺負希茲娜，但從沒打算讓她感受到如此的不安。
倒不如說，如果她真的很討厭的話就乾脆饒過她。
但是，自己雙手持續不斷感受到的心跳，這難道不是回應著莉雅動作而產生的反應嗎。

「是不是真的喜歡，這種事情我不清楚⋯⋯但是打從出生以來，希茲娜妳是第一個讓我打從心底想抱著上床的人。」
「真的？那露露呢？」
「她像是家人一樣的存在。頂多是爾偶揉揉胸部而已」
「莉雅也⋯那個⋯第一次嗎？」
「是阿，我也只是光有著知識阿」
「是、是這樣阿⋯」

希茲娜的身體不再僵硬著。
在莉雅的手中變的極為柔軟。

「如果是這樣的話，可以呦」

希茲娜已經停止流淚了。

「莉雅的話，可以呦」

隨著衣物摩擦的聲音，莉雅也將衣物脫去。

「手稍微舉起來一下」
「嗯」

雪白的肌膚暫露無遺。

「莉雅⋯⋯」

希茲娜的雙手環抱住莉雅的脖子。
莉雅那豐滿的雙乳與希茲娜的乳房緊貼在一起。
既柔軟又溫暖，希茲娜從來沒有想過兩人的肌膚如此緊密貼在一起的感覺會是這樣。

「雙腿稍微張開一點」

莉雅的手指劃過希茲娜的大腿內側，接著碰觸到那柔軟毛髮下的私密處。

「啊⋯⋯」

激烈的喘息聲中混入了一絲嬌喘。

「別擔心，會好好弄濕的」
「討厭⋯⋯」

莉雅用著溫柔的手法持續欺負著希茲娜的性感帶，偶爾規則的動著，偶爾不規則的動著，偶爾改用彈的，讓希茲娜的眼眶不自覺的充滿著淚水。
而希茲娜的指甲也在莉雅的背上畫出一道道痕跡，這些都是甜蜜的痛苦。

「啊，手指⋯⋯」
「別擔心，只是放進去一點點而已」

中指規則的動著，拇指則是不規則的動著・左手一邊溫柔的揉著胸部，偶爾也會輕捏乳頭。
此外還持續不斷的接吻著。

「那麼，伸進去兩隻手指咯」

那股帶著快感的疼痛，讓希茲娜的嬌喘聲變大了起來。

「還好嗎？」
「雖然會痛但感覺很舒服⋯這太奇怪了⋯」

希茲娜也一邊吻著莉雅的肩膀和脖子。

「莉雅也⋯⋯濕了嗎？」
「感覺非常的舒服喲」

對於莉雅坦率的感想，希茲娜不由得笑了。

「那、這個、稍微⋯雖然還是會痛，但我會忍耐的」
「嗯」

就這樣莉雅用手指奪去了希茲娜的第一次。

很可怕。
莉雅給人的感覺很可怕。
但更害怕那個人不再喜歡自己這件事。

「那個阿，可以答應我一個要求嗎」
「嗯？」
「不可以跟剛剛那個女孩做這種事」
「這種事？」

莉雅使壊的動著雙手。
接著靠在那眼中帶淚的希茲娜耳旁低聲呢喃著。

「我答應妳，不跟她做呦」

觸れ合って。
抱き合って。
重なり合って。
愛し合った。

───

希茲娜隨著舞劍的聲音醒了過來。
不過實際上舞的不是劍，而是刀。

小屋中很昏暗，但是魔法產生出來的燈光照耀在全身赤裸的希茲娜身上。
一看見昨晚遺留下來的慘狀後，讓希茲娜的心情不由得暗淡了下來。
不管是誰只要看見這場景，大概都能理解發生了什麼事吧。

「莉雅⋯」

藉由窗戶小聲的呼喊著。
喊著那名正在舞著刀的美人。
莉雅查覺到後便回到房內，看到床單上沾染的片片血跡，不由的撓了撓臉頰。

「洗一洗就好啦？」
「但是給人看到會覺得很羞恥阿」
「嘛，那就用魔法好了」

藉由洗淨的魔法以及乾燥的魔法，大致上將痕跡都去除了。但床單反而顯得太過乾淨了也不一定。

「這樣就可以了吧，妳的身體不要緊嗎？」

莉雅很平常的問著。但是接話的希茲娜心情仍平靜不下來。

「嗯，因為有接受治療魔法治療了」
「不要太過勉強阿」

這樣說著的莉雅，卻往往是那個亂來的人。
希茲娜笑了，像花一樣綻放出少女的笑容。

「欸莉雅，來當我練劍的對手吧」

於是兩人像是要互相吐露愛意一般用劍相交著。