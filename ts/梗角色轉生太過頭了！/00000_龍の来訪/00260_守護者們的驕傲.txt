在娜哈特的手上，小小的蜥蜴正蠕動著。
全身是透明的黑色，那是像受到光後產生的影子一樣的色彩。

「呀啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊！」
「『『呀吼哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦！』』」

悲鳴和歡聲同時響起。

姑且對發出歡聲的無禮之徒用風魔法迷了眼睛。
背對叫喊著眼睛，眼睛啊啊啊啊的冒険者，娜哈特把布披在黑裝束的少女身上後，將黑影的生物放到了空中。
放蜥蜴的前方有娜哈特創造出的紅蓮。被放入應該說是地獄之釜的業火中的蜥蜴一瞬間就像蒸發一樣燒盡了。

「剛才的，到底是什麼？」

尼古爾多問娜哈特。

「影魔法（Shadow Magic）──潛於影間的追跡者（Deep Chaser），吧。呋姆，艾夏的話好像是對的」
「呋欸？」

艾夏睜大眼睛驚訝了。

「靠在這裡的人們解決不了，這句話。現在知道敵人是馬馬虎虎的好手了。公會長喲，這個街道已經被發現了，時時刻刻都被瞄準著吧」

尼古爾多冷靜地把握娜哈特的話。
雖然是打算進行調查的，可那影的生物就是說現在反而提供了情報吧。毫無疑問是失態。
楓花和凱特都以沉痛的表情低下了頭。
因為他們什麼都沒有察覺，好像輕易地為敵人帶了路。

但是，公會長認為那是自己的失態。以為派遣豪斯曼是最好的。可是，連那個都應該說是估計得太天真了吧。
娜哈特在每一個人都陰郁起來時，用只有公會長能聽見的聲音說道。

「你們什麼都不做也行哦。因為僅限這次我決定行動了吶。所以，即使你們什麼都不做我也可以一個人解決一切」

娜哈特像沒什麼大不了一樣地說道。
對那自大得不講理般的自信，尼古爾多快被壓倒了。
看起來絶不是在說謊。
她一定有著言出必行的把握。

尼古爾多想起了前幾天降下的竜。當初叫來娜哈特的首要理由是為了打聽出用那個強大的魔法的是娜哈特嗎，如果是的話使用的理由是什麼呢。不過，那種事已經沒有問的必要了。
眼前的少女擁有著印證言語的力量。那個竜肯定也是她對疏忽的人的報復。

那樣的少女說會解決一切。
對本來應該豁出命守護街道的尼古爾多這幫冒険者，娜哈特說叼著手指看著吧。

可是，真的那樣就好嗎？
在尼古爾多的心中，不成言語的混沌的思想奔波著。

現役的時候擁有即便是在Ａ級冒険者中也被稱為最高峰的實力，現在作為掌權公會的會長，尼古爾多有著將這個街道，將公會的伙伴，將家族，將市民從魔物的威脅下守護了下來的驕傲。

尼古爾多小的時候冒険者公會是更加小的組織。不違背來自貴族的命令作為便利屋被使用，被錢吸引的人是像代替騎士驅散魔物的威脅的棄子一樣的存在。那只是作為貴族們為了保護自己的名譽的行為在利用冒険者公會，絶不是保護家族，保護市民的行為。
那個時候還有的外圍的貧民窟的人們和周邊的小村莊被見死不救的事也有過很多。對貴族們來說應該保護的是交易都市這個據點，裡面的東西無關緊要。

所以，尼古爾多改變了。
只把力氣作為武器，無力者才要被保護的組織，一步一步努力接近的結果便有了現在。
也有朋友贊同那樣的尼古爾多。
只讓那樣的朋友行動，現在陷入了生死不明。

儘管如此，卻把繮繩交給誰，就那樣悠閑地在這裡看著嗎。
在一切結束之前，在安全的地方呆著嗎。
那種事，尼古爾多的矜持不容許。
將心中涌現的憤怒作為活力，尼古爾多大喊。

「聽老夫說！聚集在我們公會的諸位冒険者喲！現在，交易都市，我等的故鄉陷入了空前的危機！事到如今就不隱瞞了，敵人恐怕是被封印在泉水下的古代魔族吧！」

因為尼古爾多的聲音，公會全體一下增添了寂靜。
對古代魔族這個名稱，開玩笑吧，不可能，這樣的小聲變成恐懼傳播了。
娜哈特愉快地看著尼古爾多。

「我等現在迎來了決斷之時！也就是戰鬥或者逃跑，單純且明了的唯二的選擇！」

尼古爾多環視公會。然後，將銳利的目光朝向了冒険者們。

「公會會提出委託，但不強制！諸位的意思才是比什麼都優先的！
希望守護這個街道，守護公會，守護家族的人憑自己的意思拿劍！
逃跑，戰鬥，選擇哪邊都是諸位的自由。自由正是作為冒険者的證明。老夫只是想自由地，憑自己的意思保護這個街道而建立了現在的公會，所以一點也不打算讓這裡崩潰！

聽好，聽仔細了，我的孩子們喲。除了力氣一無是處的混蛋們喲。
不管選擇哪邊老夫都平等地告訴你。
不要浪費你的命！不要浪費無可取代的命！絶對！賺錢之前首先保證命，賭命之前首先磨練力量，老夫相信傳授給諸位的我們公會的教導會保護諸位的命！

現在是決斷之時，我的孩子們喲！

老夫作為諸位的會長，絶不會讓諸位白白去死！絶不會將諸位送去送命的戰場！
正因為如此，娜哈特小姐──」

看著冒険者們的視線轉向了娜哈特。
然後，從冒険者們中間發出了驚愕之聲。
尼古爾多向娜哈特筆直地低下了頭。

「──請把力量借給無力的我等」

尼古爾多的話也是沒有虛偽的發自真心的話。
對沒有緊跟約定會解決事態的話，而是憑自己的意思、想法得出的結論，娜哈特誠摯地抱有了好感。
而且，娜哈特的意思在被艾夏說教後約定的時候就定了。事到如今可以說沒有被拜託的必要。

「我也拜託了，娜哈特小姐」

克麗絲塔並排公會長那樣說。
那個潮流，重大的心思不斷傳開。

「『『拜託了！！』』」

注意到時，不知什麼時候公會的大家都並排公會長低下了頭。
娜哈特的實力他們也看得清清楚楚。
那正是希望。
思想合為一體集中在了娜哈特播種的小小希望上。

受到那樣的視線後，要說作為本人的娜哈特的話────露出了弛緩得散漫的，像孩子一樣天真的笑容，滿臉的笑容。
那是簡直就像惡作劇成功的孩子一樣，而且被推舉為班級委員很高興那樣的微笑。

「庫庫庫庫，庫啊哈哈哈哈哈哈，呋哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈，放心吧，你們──」

娜哈特僅僅只是感到了高興。
因為無論怎麼說。在遊戲時代誰都不會從心底想要依靠桀驁不馴的娜哈特的話。那也是當然的，不僅周圍在娜哈特之上的強者也有很多，而且大家有時會像保護孩子一樣看著娜哈特。

例如，如果是雙子的貓耳少女們的話，

『啊哈哈，又交換了呢，娜哈特醬』
『不要，太前面。很擔心』

大概會這樣說吧。

如果是那個沒氣力的效率主義者的男人的話，

『啊，雖然無所謂，但請趕快幹呢』

會這樣說，

『你這傢伙，怎麼對公主說話的！』
『請放心，我等親衛隊會保護公主的！』

親衛隊肯定會這樣頂撞。

那裡大有對娜哈特的庇護，雖然信賴著娜哈特，但依靠這種感情只是很稀薄的。雖然對娜哈特來說那是很高興，但如果提更高要求的話，這種，怎麼說呢，希望更加更加被大家依賴。那是自我表現欲很高的娜哈特般的願望。知道不可能卻如同發出的話那樣辦事正是娜哈特的願望。

正因為如此，娜哈特得意忘形了。
心情非常好，娜哈特取出了本來大概不需要的那個──

剎那間，黑暗來臨了。
籠罩大廳的所有光──被娜哈特穿的那個吸入了。不讓誰看見主人的裸體，暗之衣覆蓋了娜哈特。

古代級（Ancient）防具──原初的深暗（Darkness）獰猛地吞食了光後僅僅只留下了看不見前方的黑暗。

在那樣的黑暗中聽見了心臟跳動。
像生物一樣在半空流動的漆黑的衣服將娜哈特的身體包了進去。如果說宵暗的擁抱（Night Breath）是娜哈特的私服的話，原初的深暗（Darkness）就是對娜哈特來說的戰鬥服。如同天女穿上羽衣一般，娜哈特在黑暗中穿上了衣服。

然後娜哈特取出了另一件裝備。
充滿黑暗的空間再次被七色的光照亮了。
在透明的球體中，淡淡的虹色的光搖動著。

七元世界的寶珠（Seven Sphere）

那正是娜哈特持有的武具。
不過，乍一看不會覺得那是武器吧。簡直像珍珠一樣的球體與其說是武器，形容為寶石也許還比較正確。

輕飄飄地在空中舞動的寶珠的光輝以娜哈特為中心捲起漩渦時，光再次回到了世界。

困惑與驚愕合一的視線被投向了娜哈特。
不，應該說是被集中了吧。
穿著像帶自由意志一樣的黑暗，浮著神聖的寶珠的現在的娜哈特有著甚至有人錯覺她活像五大神的幻想般的美麗。
但是，和那樣的神秘的光景相反，果然娜哈特一直露出著和容貌象徵的年齡相稱的笑容。

「────只要有我娜哈特醬在，就如同勝券在握一樣！你們只要和與自己的力量相抵的對手戰鬥就好。奢侈一下想成是賺經驗值和錢的絶佳機會去狩獵魔物吧！」

（插圖００６）

過去，曾背對同伴去狩獵。
儘管那個時候是遊戲中，可即便是在遊戲中娜哈特也確實賭上了性命。
所以，稍微能明白他們的感受。
不安也好，恐懼也好，稍微能夠理解。
正因為如此，娜哈特像往常一樣承擔了先鋒。
小小的背上承載著確實的希望。

娜哈特現在興奮絶頂。
但是，後來後悔了。
聽任興奮與歡聲，無意說漏了嘴的話要是沒說就好了。

「好好，那麼作為褒獎，我就給最活躍的人做膝枕摸頭吧！」

不經意的一句話。
空氣凍住後，歡聲又再次大起。慾望的聲音交織在一起，娜哈特甚至在享受著邋遢的男人們的下流的話。

原以為那是經常開玩笑地對親衛隊說的話。
娜哈特沒有想到那樣的話成為了小小的少女才能開花的契機。