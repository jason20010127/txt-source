“——唔啊！？”

一瞬間，我失去了意識。

“嗚，呀”

但莉莉好像也一樣。

“……喂，莉莉”

“怎麼了，黑乃？”

我呈大字型仰面倒在地上，而莉莉則緊緊地抱著我，把臉頰貼在我的胸膛上。

“你沒事吧——不對，你醒了的話就趕緊離開啊。”

“對不起，不知不覺就”

誒嘿，看到可愛的笑著吐舌頭的莉莉，我不由得忘記自己確實是在戰鬥。


我綳緊鬆懈下來的心，為了不讓零距離的莉莉逃跑，而朝她使出了魔爪。

為了從背後將莉莉綁起來，黑色的鎖鏈咔啦咔啦的一起飛了出來，但在那之前，她用手一推我的胸膛，輕輕地飛了起來。

“謝謝，托黑乃的福，我一點傷都沒受。黑乃果然很溫柔呢”

鮮紅的妖精結界再次遮蔽住莉莉的身軀，同時，也粉碎了衝向她的鎖鏈。

果然只靠魔爪是不無法抓住光之結界的嗎？

“……這是城堡裡啊”

由於暫且拉開了距離，我快速地確認了下周圍。看來我們好像撞破城堡的天花板，掉了下來。


從正門進去後，在前方展開，就是城堡的入口大廳。其大約有三層樓高，以其寬度和高度，雖在室內，確是個很有開放感的大廳。當然，因為是為了迎接樂園客人的區域，所以與剛才我偷偷潛入的背面不同，風格極盡奢華，內部裝修十分華麗。


可是，由於我和莉莉像隕石一樣砸進來的原因，地板破碎，雕像坍塌，大吊燈也碎裂，情況變得十分糟糕。

“呵呵，歡迎來到我的城堡。這裡十分漂亮，我本來打算以後就在這裡招待客人的”

“不好意思啊，弄得亂七八糟的。”

在這樣混亂的入口處，莉莉在大廳的中央優雅地鞠躬，看到這建造的十分豪華的城堡，竟讓人產生種這裡有著真正的歷史般的錯覺。


這裡是室內，她的飛行能力受到限制。雖然也可以強行沖破牆壁，但我不會放過這種破綻。雖然面積相當大，但是在被分隔開的空間內，我有著強大的近戰能力……本應如此，但莉莉卻這麼從容，果然，她有著什麼拉近差距的手段呢？

“不過，我仍表歡迎。衷心感謝大家的來訪”

啪,莉莉一打響指，接著就響起了密集的腳步聲。

從同時被打開了的門中出現的，仍是，拿著軌道槍穿著動力服的人造人兵。他們像特種部隊一樣的整齊劃一的涌入大廳，連環繞在二樓、三樓的走廊上，也密密麻麻地擠滿了人。


幾十個槍口一起對準了我。

“竟然有這麼多人……”

“想創造出理想的樂園，是需要很多人的啊。”

原來如此，對於莉莉來說，人造人是為製作金字塔而搬運石頭般的好用的勞動力啊。不管她再怎麼聰明，也只有兩只手。能把香格里拉和迪士尼樂園維持在最低限度的運行狀態，正是因為有他們的體力勞動。


這樣啊，我服了。真正有人控制的人造人軍團——

“射擊”

他們效仿著手持兩把手槍的莉莉，如忠實的玩偶兵般按 命令一起扣下了扳機。

面對蜂擁而至的軌道槍的雷彈，和莉莉的光線，我只好拚命避開。

“可惡——黑牆”

為了逃脫包圍，先要從大廳中出來。以莉莉和人造人軍團為對手的話，這個入口大廳也太寬敞了。要稍微狹窄點的地方更為合適……

我以前傾姿勢躲過射線跑出去時，使用出了防御魔法。朝著準備逃走的門，製作出了狹窄的像通道一樣的兩面牆壁。

以這個發動速度，和這個大小。所以強度自然會下降,不過，只要能擋下一發軌道砲就夠了。

在喧雜的石牆被無數的槍彈打碎的巨大聲響中，我跑過自己製作了的作為障礙物的通道。

“哇哦！？”

在快要跳進門前時，兩道光束射穿黑牆從背後追了上來。這種程度的防御可擋不住莉莉的光束，她瞬間就切開黑魔力製造出的牆面。

“……真險啊，我真的會死的啊，莉莉”

我在最後關頭滾進門中，躲過了光束。

但是，想安心還為時過早。莉莉馬上就帶著大量部下追趕著我。

"混蛋，到底有多少人啊！”

從大廳出來的通道前方，出現了恭候多時的人造人兵擋住了我的去路。

“滾開！”

我穿過軌道砲的槍火，強行突破。即使不回頭，莉莉那巨大魔力氣息也如芒在背。

“追”

“yes，my princess”

在游樂園的城堡裡捉迷藏吧。是小孩會心歡雀躍的場景吧,但我卻只想著活下去。

我也不知道城堡詳細的內部構造，所以為了尋找對自己有利的地方，我開始全力奔跑。



---



在城中奔跑了多少時間呢？逃跑中強行突破時也打倒了一些人造人兵,不過，看不出他們的數量有減少的樣子。雖然不可能數量無限，但果然還是有著相當多的人數。


而且，他們的性能也並不差。那個動力服風格的裝備，看來真的有強化身體能力的功能，我拔劍砍他們的時候，的確感覺到他們的力量有普通人的三倍左右。

而且他們不是單純有身體能力而已，其戰鬥的動作，與我知道的活屍大相徑庭。以這個性能來看，每個人能比得上等級3的冒險者吧。


在莉莉的率領下，被這些傢伙大舉追趕……怎麼可能抓不住我呢。

或者，我只是被誘導了吧。

“哈……哈……這裡就是王座之間嗎……”

盛大的捉迷藏迎來了終結。我已經跑到最上層的最深處的房間了，也就是說，再也沒有可以逃走的地方了。


無論怎麼看這被我踢破巨大的雙開門後，衝進來的房間，都只能叫到王座之間。而且，我還有進過斯巴達王城和米婭的夢，兩個都是真正的王座之間，沒錯。

果然，這個王座之間也是面向觀眾的區域，和入口大廳一樣，不，其內部裝修更為華麗。嘛，說到城堡中最豪華的地方，肯定就是王座之間或者舞廳。


與之相比，米婭那黑色的王座之間就顯得非常煞風景。但到現在，我已經不想再被叫到那個空間了。

“這裡的話，好像正適合啊。”

“追逐遊戲已經結束了嗎？”

領著強大的騎士們，莉莉悠然地出現在王座之間。

我站在王座的正前方，凝視著以敏捷的動作形成包圍的人造人兵。莉莉則背對著門，正面注視著我。

“是啊，就到這裡結束吧。”

“……喂，黑乃只要坐在那個座位上，接受我的話，一切就都結束了”

因為莉莉她知道我的答案等吧。所以她是以帶著少許悲傷的表情說出了這句話。

“這個王座是我的座位嗎？”

“嗯，因為黑乃是樂園之王。”

“莉莉是女王啊，我還以為我是奴隷呢。”

“呵呵，我才是愛的奴隷。”

真是笑不出來的玩笑。

“在現在的話，還可以漂亮的結束。我不想傷害黑乃，也不想讓你痛苦”

我也不想受傷，不想感到痛苦啊。

但是，更重要的是，我的心卻無法認同。

“你想要我。我也想要你。我倆的願望是絕對無法相容的。所以，唯有戰鬥”

我和莉莉都不願讓步。

“別放水了，莉莉。來吧——如果想要我的話，就算砍斷手腳，也要弄到手”

“恩，對不起，黑乃——我會治好你的傷的。”

莉莉笑著下達了射擊的命令。

我已經無處可逃了。

但我也不打算逃避。

因為，我已經找到了與我所要求的條件相稱的廣闊場所。

“歌唱吧——《行世之死》・《反魂歌之暗黑神殿》”

我反手握住從影子中召出的漆黑薙刀，當場用力插向地板。漆黑的詛咒之刃刺向地板，發出清澈的音色……然後，開始歌唱。



啊啊啊啊啊啊啊啊啊啊啊啊！！



刺耳的女性尖叫在王座之間回響。這令人無比頭痛，心覺不快的叫喊，卻確實演奏著旋律。

而當你聽到這個聲音時，就為時已晚。

因為聽了行世之死唱這首歌的人都瘋了。

“啊！？這是……”

在聽到瘋狂的歌聲前，莉莉突然堵住了耳朵。雖說她只是擺了一個用雙手遮住雙耳的姿勢，實際上阻斷住詛咒歌聲的，應該是那愈發閃耀的妖精結界吧。

果然，對莉莉是沒用的啊。可是，其他的人又如何呢。充其量只強化了身體能力強化，並只有少許物理·魔法耐性的動力服的人造人兵們。

“哦，啊……”

“啊，嗚，啊啊啊啊啊啊啊！！”

人造人兵突然大叫起來。本是好好對準我的槍口，也像找不到了目標一般搖晃起來，並且，甚至有人朝著目標的反方向開槍了。

這槍聲就像是開始的信號，他們手中的步槍一起射擊了。

“哇啊啊！”

“啊啊啊啊啊！！”

本除了回應以外不會發出任何聲音的死寂的人偶兵們紛紛發著尖叫和怪聲，舉著手中的槍。一直摁著全自動的扳機。雖然不知道裡面裝了幾發子彈，但他們只顧著將炮火胡亂的傾瀉向四周。


好像在和完全看不見的敵人作戰，準確的說，應該是周圍的一切看起來都是敵人。因此，站在旁邊的伙伴，作為目標的我，以及作為主人的莉莉，在他們眼中都是應該要殺掉的敵人。


莉莉用一擊將把連射的槍口對準她的人造人兵給幹掉了。

“...混亂的異常狀態……不，這是……被惡靈附身了呢”

“的確是正確答案。”

這《反魂歌之暗黑神殿》是我通過與《行世之死》對話而得到的新魔法。我並不清楚這一招到底是黑魔法還是屍靈術？，但這肯定是以歌為媒介的一種結界。


其效果是在歌聲響起的範圍內製造出大量惡靈，創造出可怕的不潔領域。

存在於那個地點，或者是不知從哪裡被叫出了來的惡靈，會因為歌聲而活性化，有屍體的話，就會俯身進去，使其變為不死族，是活人的話，就會憑依在其身上，讓他喪失理智


在守墓之歌回響的王座之間，已經能夠看見很多骷髏或是漂浮著的半透明的人型惡靈了。這些惡靈們不斷實體化，但只要碰觸到莉莉的光芒就會瞬間消失，即使接近我也會被彈開，完全無法干擾我。


但是，對於這沒有精神防御手段的人造人兵來說，當他們被一隻，二隻，三隻，到更多惡靈一同附身的話，轉瞬間其身心就會被怨恨所支配。

“這樣一來，忠誠的騎士就變成了野生的不死族了。”

這就是對付效忠莉莉的活屍的對策。菲奧娜在最後關頭，因為他們而輸。而我在精疲力盡的時候，要是被他們襲擊的話也就結束了，雖然像現在這樣被他們和莉莉一起襲擊也十分難纏。


所以，我準備了將他們一網打盡的對策。


只要這《反魂歌之暗黑神殿》持續歌唱，莉莉就不得不孤身戰鬥。這個王座之間的面積，正好是歌聲能一直維持結界的大小。即使增援從門外衝進來，也馬上會被惡靈纏身，失去理智吧。


這樣就可以排除掉礙事者了。因為能保持理智站在這裡的就只有我和莉莉兩人。

“是啊，這樣的話，就已經不需要了——自爆術式 解放”

碰！隨著爆炸聲，人造人兵的頭部一齊爆炸開來。他們的頸部被炸得稀爛，冒著煙倒了下來。無頭屍體就這樣倒下，不知是動力服的功能嗎？我看見他們殘留的身體也被分解成了像白灰一樣的東西。


然後，就只剩下乾淨的動力服和乾燥的灰堆了。

真是果斷的處理掉了啊。我想起了那能流利說話的愛因，他們應該擁有著和人一樣的思考回路和智慧吧，但莉莉竟然能毫不猶豫地殺掉他們……這就是莉莉殘酷的一面吧。


但是，我卻不可思議的沒有對她感到厭惡。果然，我對莉莉太過包容了。

“已經沒有礙事的傢伙了。這裡只有我和你……來吧，莉莉”

右手握『首斷』，左手持『雙鷹』。

沒有魔王加護。但是，力量就像從靈魂深處中涌出一樣，從我的身體裡迸發出漆黑的靈氣。

“來吧，黑乃。請接受我的全部”

與我想對的莉莉依舊拿著兩把手槍。展開蝴蝶之翅，纏繞其身的真紅的妖精結界光芒大作。

“……”

沉默。在王座之間對立的我和莉莉，像時間停止了一樣靜止不動。


也許我們心中也在想，要是時間就這樣停止就好了，因為之後要就要迎來可怕的結局。


所以，這個沉默，對我們彼此來說，一定就是最後的躊躇。

而且，對於已經做好覺悟的我和她來說，這種迷茫也只是一瞬間的事情。

“——破啊！！”

我動了。由我先動。


眼前，有自己從心底想要的存在啊。還有什麼好猶豫的。她離自己如此之近啊。


啊啊，已經，只能看見莉莉了——
