# novel

- title: 昏き宮殿の死者の王
- title_zh1: 昏暗宫殿的死者之王
- author: 槻影
- illust: メロントマリ
- source: http://ncode.syosetu.com/n6621fl/
- cover: https://images-na.ssl-images-amazon.com/images/I/817Qiy0iKiL.jpg
- publisher: syosetu
- date: 2019-05-18T16:37:00+08:00
- status: 連載
- novel_status: 0x0100

## sources

- https://kakuyomu.jp/works/1177354054888541019

## illusts


## publishers

- syosetu

## series

- name: 昏き宮殿の死者の王

## preface


```
――我不想死。我想要自由。
若是為此，我――甘願成為『怪物』。

給全身帶來永無止境的劇烈疼痛，令人衰弱最後造成死亡的奇病。
患此病的少年經歷了數年的痛苦，最後甚至沒有餘力感受絕望，在無人看護的情況下結束了人生。

然後少年再次蘇醒之時――卻由邪惡的死靈魔術師用力量變成了最低級的不死族，『死肉人』。

少年得到了感受不到痛苦的身體，歡欣鼓舞，但又馬上發現自己現在的處境和之前被支配，軟禁在病床上的時候沒什麼差別。

但是世界並沒有放棄這個追求平穩的的少年。

用死靈魔術讓少年從屍體中復活，並給予恩德之名的死靈魔術師；
對屬於黑暗之物窮追不捨，拼命殲滅的終末騎士團；
把眾多魔物收作手下，君臨各地進行爭奪的魔王們。

目的是生存和自由。必要的是力量和謹慎。

這是，追求自由，有時戰鬥，有時逃跑，有時膽怯，有時躊躇，膽小的死者之王的故事。

---------

――死にたくない。自由が欲しい。
そのためならば、僕は――甘んじて『怪物』になろう。

全身に絶え間ない激痛が奔り、衰弱の末死に至る奇病。
それに冒された少年は数年の苦痛の末、絶望を感じる余裕もなく誰にも看取られることなく生を終える。

そして再び目覚めた時――少年は邪悪な死霊魔術師の力により、最下級アンデッド、『死肉人』となっていた。

念願の痛みを感じない身体を手に入れ、歓喜する少年だが、すぐに自分の立場が未だ支配され、病室に軟禁されていた頃と大差ない事に気づく。

ただ平穏を求める少年を、世界は放っておかなかった。

死霊魔術により死体から少年を蘇らせ、エンドと名付け支配せんとする死霊魔術師。
闇に属する者をどこまでも追い詰め、滅する事に命を賭ける終焉騎士団。
多数の魔物を配下に収め、各地に君臨し覇を争う魔王達。

目的は生存と自由。必要な物は力と注意深さ。

これは、自由を求め、時に戦い、時に逃げ出し、時に怯え、時に躊躇う、臆病な死者の王の物語。
```

## tags

- node-novel
- R15
- syosetu
- だけどあまり暗くない
- アンデッド
- ダークファンタジー
- ハイファンタジー
- ハイファンタジー〔ファンタジー〕
- ファンタジー
- 呪い
- 変異
- 成長
- 戦い
- 死霊魔術
- 残酷な描写あり
- 現地人主人公
- 進化

# contribute

- 贺神镜
- 秋葉怜
- 幻靈
- 

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n6621fl

## textlayout

- allow_lf2: false

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n6621fl&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n6621fl/)
- [昏暗宫殿的死者之王](https://masiro.moe/forum.php?mod=forumdisplay&fid=161&page=1)
- 

