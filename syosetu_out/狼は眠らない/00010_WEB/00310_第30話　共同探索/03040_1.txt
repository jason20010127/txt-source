斡旋所在總合嚮導棟裡。總合嚮導棟的場所，就近在迷宮入口正面。

總合嚮導棟的構造是由四個建築物集合而成。

深處的建築，是迷宮事務統括官所。是統括所有設施的部門，擁有強大的權利。
厚重的石造建築，前方的兩棟是平房，但有著應該為兩層樓或三層樓的氣派構造。深處的迷宮事務統括所的中央部分為三層樓，是充滿威壓感的建築，能從二樓跟三樓遠望迷宮入口，還能一眼環視圍繞迷宮的設施群。

前方的三棟，在中央的是嚮導所。除了迷宮入場的許可證販賣和各階層地圖之販賣外，信紙和行李之發送，行李之保管，冒険者會需要的情報之販賣等等都不用說，也有在斡旋符合預算的宿屋。總之有什麼不懂就在這裡問吧，在最初到的一天被這麼說了。

前方左側的建築是迷宮管理事務所。是擔當迷宮內之管理和安全維持之部署，為冒険者之間的糾紛進行仲裁或斷罪，會提出物品和魔石的交貨委託。也會提出安全維持相關的委託。如果在迷宮內找到的遺物不自己保留的話，在此申報就會處理。
然後前方右側的建築是斡旋所。是接下來要去的地方。

通過中央的入口，正面是嚮導所。

也因為是在早上的時間帶，排在嚮導所裡的櫃台沒一個是空的，不論哪個櫃台前都排著冒険者。
宛如要包圍排列，眼神毫無大意的〈鼠〉們正聚集在一起。年齡有從十五歳左右到應該年過五十的男人，有各種年齡層。

櫃台之一的職員正好舉起手大聲呼喊。

「帶路希望！十五階層到十八階層！」

馬上有約十個〈鼠〉舉起了手。

職員仔細眺望舉起手的〈鼠〉們，叫了一個人的名字。

「柯古森！」

被選到的男人靠近櫃台，和職員及冒険者打招呼。畢竟是淺階層，帶路費會很便宜吧，總之是找到了工作。還能再賺多少，就看男人自己的才智了。

並排的櫃台的裡頭有許多桌子，大量的職員在坐著工作。其周圍各處排著架子，堆積著箱子，裡面收著多得可怕的文件。

在更裡頭的高了三段階梯的場所有個豪華的大桌，坐了一位穿著怎麼看都有權威的服裝的年輕女性。從那場所的話，能眺望整個迷宮管理事務所、嚮導所和斡旋所吧。女性的斜後方有個騎士在待命。其後方有扇緊閉的門，那扇門的另一側就是迷宮事務統括所。
從迷宮側看的話，迷宮事務統括所會是在裏側，從這城鎮的正門之西門筆直連接到迷宮的巨大道路的終點，就是這迷宮事務統括所，從正門側看到的迷宮事務統括所，具備壓倒看到的人的威容。然後，能利用迷宮事務統括所的只有貴族。

雷肯側眼看著嚮導的狀況，跟在阿利歐斯的右邊走著。
穿過林立的石柱，通過嚮導所之棟和斡旋所之棟之間的拱型巨大出入口。

這裡是斡旋所。
聚集了冒険者們。在這裡的幾乎是單人冒険者。想盡快湊齊人數的話，能到這裡湊齊。但是單人冒険者經常讓人期望落空。能信用的果然是有實績的隊伍。有斡旋希望的隊伍，會在這申請後到宿舍待機，或是做別的事。在找到符合條件的隊伍之前，有時也會花上好幾天。

深處的櫃台沒人，雷肯便靠近那邊。

坐到放在櫃台前方的椅子上，雷肯開了口。

「在尋找隊伍成員的補充。單人也行隊伍也行，要能馬上潛入迷宮的人」
「這裡是深層用的窗口喔。那麼，幾階層？」
「從八十二階層的大型個體的房間開始」
「喔」

男職員抬起頭看了雷肯。有張瘦得露出青筋的臉的初老男性。雷肯比較高，所以得抬起頭。

「說說你的隊伍的隊名、人數、構成」
「隊名是〈虹石〉。人數是兩人。構成是什麼？」
「職種、擅長武器、擅長技術」
「兩人都是劍士。我能使用〈回復〉」
「喔，真稀奇。〈回復〉持有者能去八十階層帶的話，有八件不錯的斡旋是可能的」
「沒打算被僱用。由這邊僱用」
「你說，僱用？不是聯合探索，是僱用？」
「聯合探索和僱用，有什麼差別」
「聯合探索的話，每個成員都是對等的。要去哪裡，何時要撤退，得到的東西的分配，都要以討論來決定。這邊不會干涉那內容。你們是兩人隊伍，所以要探索八十階層帶的話，就要接受五到十人程度的斡旋。握不住主導權吧」
「僱用呢？」
「必須支付僱用人數的日薪。不論有沒有收益。日薪金額由這邊決定。取得品的分配方法由當事人之間討論，雖然要給這邊報告，但當然是僱用側握有主導權」
「我要僱用」
「知道了。告訴我你們組過的隊名」
「在這迷宮，還沒有組隊過」
「告訴我還記得的組過的冒険者的名字」
「所以說，在這迷宮沒有跟任何人組隊過」

男職員放下了筆，抬起頭盯著雷肯。

「你是想說，靠兩人下到了八十二階層嗎」
「對」

男人一臉憤怒。

「滾出去！」
「什麼？」

職員站了起來，指著出入口怒吼。

「這裡不是你們這種人來的地方！馬上給我滾！」

（啊啊原來如此）
（這男的覺得我在說謊阿）
（這等迷宮的話就會有相當有本事的冒険者吧）
（這種人就能兩人攻略八十二階層不是嗎）

思索著自己兩人能不能現在馬上證明憑兩人抵達了八十二階層，得到了很難的結論。讓人鑑定恩寵品的話，就能證明有得到八十階層帶的物品。但沒辦法證明是憑實力得到的。更不可能證明是兩人在探索。

雷肯靜靜地站了起來。

「打擾了」

然後直接轉身，走向出入口。
騎士對把雷肯趕走的職員告知了什麼，職員便走向坐在後方的氣派桌子的女性。以〈立體知覺〉捕捉到此。

（如果是以前）
（要是受到這種待遇就會怒火沖天吧）
（我也稍微接近希拉所說的剛劍了嗎）