「⋯⋯那麼，難得盛裝打扮成這麼可愛的模樣，第一個想去的地方就是這裡啊」
「因、因為⋯⋯只要一次就好、就是想要過來看看嘛」

這裡是王都內數一數二的人氣娛樂設施。

每天都會固定舉行表演，是許多大批的遊客會從王国各地爭相朝聖的場所。
場上有兩名戰士的劍鋒相向，每當武器激烈地碰撞，台下觀眾的情緒就會隨之高漲。

這裡是闘技場。

有專屬的鬥士，有時也會有對自己自信滿滿的騎士或冒険者參加。
不僅是一對一的單人賽，團體戰和改造競技場的模擬戰也同樣具有相當高的人氣。
也有人類與魔物對戰，其賽制上的豐富變化性也是讓觀眾們不厭其煩且絡繹不絶的原因之一。

「嘛，倒是也很有珂露雪的感覺呢」
「是、是這樣的嘛？」

畢竟是屬於戰鬥民族的亞馬遜族。

「而且我也想再次來訪此處。自從上次以來也過了蠻長一段時間了」
「誒嘿嘿，這樣的話就太好了呢」

順帶一提，玖今天待在宿舍裡看家。
它就交給艾莉婭照顧了。

雖然是不想和珂露雪分開的玖，但是似乎對象換成艾莉婭就沒關係。
明明就完全不親近我的。

『先生女士們，現在即將進入本日的主賽環節！』

看來使用了魔導道具擴音，廣播的消息傳遍整個會場。

底下的觀眾們人聲鼎沸，瞬間熱血沸騰起來。

每天都會進行很多場賽事，作為其壓軸便是主要賽事。

『首先出場的是這位選手！擁有白馬王子的帥氣外表、目前第二次蟬聯本屆女性最想做為戀人的第一名對象！當然不只是高顏值！其實力也是有目共睹！
眾所皆知的、排名Ｂ級的冒険者，迪米～～斯！！』

在主持人的介紹下先出場的是，一名看起來二十歳左右的青年劍士。

金髮碧眼，配上一臉爽朗的笑容。
的確像是來自某国的王子殿下。
他只是朝著觀眾席輕輕揮手，馬上就受到女性歡聲雷動的熱烈青睞。

在那個年紀，就能達到排名Ｂ級的冒険者
倘若身來沒有才能，是不可能練成Ｂ級的程度。

那可是幾百人之中，僅有少數人才能達到的境界。

更何況他才年紀輕輕，二十齣頭而已，可說是極為出類拔萃的天才呢。

而與他對戰的則是⋯

「魔法陣？」

忽然，場上出現了巨大的魔法陣圖案。

應該是事先就設置好，剛剛將魔力傳入啟動的吧。

『而即將與迪米斯選手對戰的，竟然是由召喚魔法呼喚而來狂暴的魔物！』

看來那個是召喚用的魔法陣。
召喚魔法並非憑借個人能力水平就能使用。

這是因為詠唱及畫魔法陣的前置既花費時間，一般人類的所擁有的魔力量又不足以供給召喚魔法陣
所以通常都還會收購昂貴的魔石補足。
而像是冒険者小規模的集團，會使用召喚魔法的魔法師少之又少。

正因為是日進斗金的闘技場，才會投入高成本營運，將之完成吧。
暫且不論危險度低的魔物
要捕獲強大的魔物並帶進競技場非常困難，所以才選擇用召喚一途吧。

過不久，魔法陣發動，競技場上出現了一隻巨大魔物的身影。

『撒！出場的是⋯⋯彌諾陶洛斯？』

為什麼是疑問句？

『失、失禮了！⋯⋯誒斗、危、危險度Ｂ級的魔物！

據說具有毀滅一座小城市的戰鬥能力、牛頭人身的怪物、彌諾陶洛斯！』

噗哞哞哞哞哞哞哞！彌諾陶洛斯像是彰顯自身的存在，發出了驚人的咆嘯。

觀眾們的熱情更加高漲。

「啊咧？　彌諾陶洛斯的體毛是那個顏色嗎？」

珂露雪在一旁歪頭疑惑。

她指的應該是那漆黑色的體毛吧。

「話說回來，公會中裝飾用的剝製標本是咖啡色的呢。難道每隻個體上都有顏色上的差異嗎？」

我從來沒有親身遇過一頭彌諾陶洛斯。
基本上這種魔物只會在地下迷宮中出現，然而我連在賽蘭大迷宮中也從未見過。

在我們談話到一半時，比賽開始了。
迪米斯來到彌諾陶洛斯面前，正面對峙。

Ｂ級冒険者對上危險度Ｂ級的魔物。

這裡經常很容易被誤解，雖然都是Ｂ等級，但是兩者的評定標準並非通用。

雙方有各自的標準。

魔物很強大，這點毋需多言。

若是Ｅ級或Ｄ級，兩者差別可能沒這麼明顯，危險度等級越高，其差別越明顯。

一般來說，危險度Ｂ級的魔物，Ｂ級冒険者較不會選擇單打獨鬥，而是多人組隊進行團隊討伐。
即便如此，仍讓迪米斯與彌諾陶洛斯進行一對一戰鬥，換言之，迪米斯不只是個不折不扣的強者，而且具備獨自一人便能討伐彌諾陶洛斯的實力。

「彌、彌諾陶洛斯⋯⋯？」

連本人都這麼慌張是怎麼回事啦？

「噗吼！噗哞哞哞吼！」

而另一方面，可能是觀眾們的熱情歡呼，使彌諾陶洛斯也朝他們發出怒吼。
迪米斯趁著這個機會，往彌諾陶洛斯背後龐大的身軀砍去。

「哈哈哈啊啊！」

上啊─、快幹掉它─、迪米斯大人─！在此起彼落的女性聲援中、迪米斯向彌諾陶洛斯發出了先制的一擊──

「噗哞！」

彌諾陶洛斯突然回頭，粗壯的手腕一拳擊中迪米斯的腹部，使迪米斯直接被揍飛至競技場的另一頭。

「『『誒？』』」

觀眾們霎時間鴉雀無聲。

迪米斯用力地撞上觀眾席隔間的牆壁。

僅僅一即便讓他失去意識，動彈不得。

『呼姆。果然，那頭並非普通的彌諾陶洛斯。恐怕是其上位種黑彌諾陶洛斯吶』