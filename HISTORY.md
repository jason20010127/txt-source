# HISTORY

## 2020-02-27

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 102, add: 1 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )

## 2020-02-06

### Epub

- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( v: 4 , c: 129, add: 28 )
- [進化の実～知らないうちに勝ち組人生～](syosetu/%E9%80%B2%E5%8C%96%E3%81%AE%E5%AE%9F%EF%BD%9E%E7%9F%A5%E3%82%89%E3%81%AA%E3%81%84%E3%81%86%E3%81%A1%E3%81%AB%E5%8B%9D%E3%81%A1%E7%B5%84%E4%BA%BA%E7%94%9F%EF%BD%9E) - syosetu
  <br/>( v: 1 , c: 157, add: 157 )
- [重生的貓騎士與精靈娘的日常](syosetu_out/%E9%87%8D%E7%94%9F%E7%9A%84%E8%B2%93%E9%A8%8E%E5%A3%AB%E8%88%87%E7%B2%BE%E9%9D%88%E5%A8%98%E7%9A%84%E6%97%A5%E5%B8%B8) - syosetu_out
  <br/>( v: 3 , c: 111, add: 0 )

### Segment

- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( s: 42 )
- [進化の実～知らないうちに勝ち組人生～](syosetu/%E9%80%B2%E5%8C%96%E3%81%AE%E5%AE%9F%EF%BD%9E%E7%9F%A5%E3%82%89%E3%81%AA%E3%81%84%E3%81%86%E3%81%A1%E3%81%AB%E5%8B%9D%E3%81%A1%E7%B5%84%E4%BA%BA%E7%94%9F%EF%BD%9E) - syosetu
  <br/>( s: 78 )

## 2020-02-05

### Epub

- [重生的貓騎士與精靈娘的日常](syosetu_out/%E9%87%8D%E7%94%9F%E7%9A%84%E8%B2%93%E9%A8%8E%E5%A3%AB%E8%88%87%E7%B2%BE%E9%9D%88%E5%A8%98%E7%9A%84%E6%97%A5%E5%B8%B8) - syosetu_out
  <br/>( v: 3 , c: 111, add: 0 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 101, add: 3 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )

## 2020-02-04

### Epub

- [重生的貓騎士與精靈娘的日常](syosetu_out/%E9%87%8D%E7%94%9F%E7%9A%84%E8%B2%93%E9%A8%8E%E5%A3%AB%E8%88%87%E7%B2%BE%E9%9D%88%E5%A8%98%E7%9A%84%E6%97%A5%E5%B8%B8) - syosetu_out
  <br/>( v: 3 , c: 111, add: 0 )

## 2020-01-23

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 20, add: 0 )

## 2020-01-22

### Epub

- [魔王学院の不適合者](syosetu_out/%E9%AD%94%E7%8E%8B%E5%AD%A6%E9%99%A2%E3%81%AE%E4%B8%8D%E9%81%A9%E5%90%88%E8%80%85) - syosetu_out
  <br/>( v: 4 , c: 123, add: 38 )
- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 20, add: 1 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 1 )

## 2020-01-20

### Epub

- [原千金未婚媽媽](girl_out/%E5%8E%9F%E5%8D%83%E9%87%91%E6%9C%AA%E5%A9%9A%E5%AA%BD%E5%AA%BD) - girl_out
  <br/>( v: 4 , c: 77, add: 77 )

## 2020-01-18

### Epub

- [世界最強の後衛　～迷宮国の新人探索者～](syosetu_out/%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7%E3%81%AE%E5%BE%8C%E8%A1%9B%E3%80%80%EF%BD%9E%E8%BF%B7%E5%AE%AE%E5%9B%BD%E3%81%AE%E6%96%B0%E4%BA%BA%E6%8E%A2%E7%B4%A2%E8%80%85%EF%BD%9E) - syosetu_out
  <br/>( v: 6 , c: 38, add: 18 )



