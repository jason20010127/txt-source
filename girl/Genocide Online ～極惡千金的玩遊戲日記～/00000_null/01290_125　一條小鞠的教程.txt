「啊啊在那啊，小鞠！」
「正義，這邊這邊！」

完成設定及角色塑造之後，我在『起始之城』的噴泉廣場裡和正義匯合了⋯⋯不過與其弄成古怪的樣子，我只是將長至掛在背上的頭髮變成馬尾並染成白色，以及將瞳孔改成金色而已，這樣應該沒問題的吧？正義則是用金髮碧眼這種主流的顏色，雖然有一小束頭髮紮了在後頸上呢。

「順帶一提在遊戲裡我的名字是瑪麗，請多指教呢？」
「⋯⋯啊啊，我是叫馬薩」
「⋯⋯沒改動過呢」
「因為我沒法由正義想出好的名字，所以沒辦法啊」

雖然我也沒有改動過很多所以也沒法說甚麼⋯⋯比起這種事，華子醬說了會幫忙的熟人是誰呢？她沒有告訴過我們那個人的特徵之類的呢⋯⋯華子醬幹勁十足所以大概忘記告訴我們了吧。

「話說回來華子醬說的人是──」
「──你們就是華子的熟人吧？」
「⋯⋯請問你是誰？」

用三束頭髮紮成的稍大的辮子從肩上放了下來、穿著華麗服裝的女子突然向我們搭話了，因此正義⋯⋯馬薩警戒起來了⋯⋯因為她握著非常巨大的大鐮所以這也是沒辦法的吧？

「⋯⋯我是華子的熟人，今天會指導你們各種各樣的事情」
「是這樣嗎，對不起，請你多多關照」
「請你多多指教！」

馬薩有禮貌地鞠躬後，我也精神飽滿地打招呼了。不過是嗎，華子醬的熟人嗎⋯⋯我不知道華子醬認識了這樣的女性呢？

「我的名字是瑪麗」
「我是馬薩」
「哦⋯⋯我是布洛森，那麼立即離開這個城鎮吧」

匆匆自我介紹後，她就急急往前走了，於是我和馬薩一起趕緊追著她⋯⋯不過我們明明是剛剛才開始的，現在卻要走出『起始之城』了嗎？沒有一般的教程任務之類的嗎？

「大致上預測到你們在想甚麼了所以我率先回答吧⋯⋯教程NPC被殺了所以那是沒有的」
「『誒』」
「所以要趕快離開這個連領主的公館都沒有的城鎮去進行種種準備」

不，不是吧⋯⋯我是聽過『起始之城』落了一名玩家的手裡了，但沒想到甚至連教程NPC都被殺了啊⋯⋯⋯

「將Status叫出來看看」
「『『Status』』」

按照所說叫出來後，眼前出現了半透明的顯示這種常見的東西⋯⋯大概透過操作這個，就可以穿上裝備、從物品倉庫裡拿出東西吧。

「那是默認的哦」
「默認？」
「對，根據不同的人，也有用智能手機和翻蓋手機的⋯⋯至於不想破壞世界觀的人，則可以設定成書本或羊皮紙的。就像是這種風格⋯⋯『Book』」

在布洛森小姐的面前出現了有皮革封面氣息的的書本，它就這樣飄浮在空中，書頁在叭啦叭啦地翻動著⋯⋯好厲害，這是甚麼⋯⋯總之好酷！

「呵，種類有相當多呢⋯⋯我用卷軸吧」
「有像是二百年前的智能手機和翻蓋手機的呢，不過我用和布洛森小姐同樣的書本就好了吧」

我和馬薩兩人翻弄著顯示並將它設定成書本和卷軸⋯⋯因為也可以將叫Status出來的方法自由地改成像是布洛森小姐的『Book』，所以把那一點也更改了。

「那麼兩人的主武器和魔術的屬性是？」
「我是刀和水」
「我是弓和風」
「嗯─，姑且平衡不錯呢。我發送隊伍申請了」

在眼前出現了比Status那時較小的半透明顯示，我操作那個來接受隊伍申請並加入了⋯⋯似乎亦能做到把這個也改成信之類的各種細緻的設定，所以之後就更改吧。

「呼呼，距離下次活動還有相當長的時間，我會充分地鍛鍊你們的」
「請、請手下留情⋯⋯」
「⋯⋯看來會很有趣呢」

雖然我不知道為甚麼明明不是華子醬本人，這位女性卻是幹勁十足的，不過為了要和義姐大人關係變好，我必須努力⋯⋯而馬薩已經適應了。

「最初是，不管是合適的玩家還是NPC也好，突襲殺掉他們」
「『誒』」
「直至某種程度為止這樣做都會很好的，但是當等級超過三十五附近的話，普通地狩獵怪物的效率會更好」

看來在一開始的時候，因為人的等級比起『起始之城』周邊的怪物的要壓倒性地高，所以殺人得到的經驗值會比較多⋯⋯用突襲擊中要害的話也是能殺死比自己更強的對手的，這一點是這個遊戲特有的嗎。不過為了不讓高等級玩家刻意殺死新手來輕易地量產高的等級，一下子能夠獲得的經驗值和提升的等級等等都是有限制的，而且因為NPC是有限的，所以普通地狩獵怪物的效率會更好⋯⋯雖然這麼突如其來讓我嚇一跳了，不過既然有那樣的理由，那這樣做就好了吧？

「⋯⋯另外不要靠近叫變態紳士和叫屠殺者的玩家哦」
「？為甚麼呢？」
「因為變態紳士正正是變態，後者則是真正的瘋子──」

從變態紳士這個名字看來，就是那個⋯⋯只有討厭的預感，所以或許應該是必須要避開的。而叫屠殺者的人，記得確實是個危險的人，我好像在調查這個遊戲的時候看過這個名字的⋯⋯⋯

「──而且是我的獵物哦」
「『⋯⋯』」

雖然如此說道並猙獰地笑著的布洛森小姐似乎非常高興，不過她卻露出了好像帶有嫉妒和憤怒等等的負面感情的複雜的表情。


▼▼▼▼▼▼▼

作者的話：
我會間或地夾雜進小鞠醬的視點的，也許這樣會導致故事的節奏稍微變差，還請諒解一下。

收到評論真的很開心呢！

然後是漢尼斯的隊員的克林的人物設計！

（圖片００１２５─０１）

怎樣呢？傳遞出輕佻的感覺呢，這傢伙可是承受了麗奈的『秘技・胡桃夾子』的。

（註：第２９話的劇情）

沒有顯示出來真是對不起！已經修正了！

（註：應該是指一開始投稿的時候並沒有顯示出克林的人物設計的圖）