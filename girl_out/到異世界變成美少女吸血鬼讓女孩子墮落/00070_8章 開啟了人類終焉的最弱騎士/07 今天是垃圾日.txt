莉莉其實不想去見其他騎士，也不願意報告關於閉門不出国王的那完全沒有進展的事情。

但是，從最初的會議開始已經過去數日了，不能再這樣沉默下去，她做好了這樣的覺悟。
就這樣，七名騎士聚集在了胃痛的騎士團長身邊

「⋯⋯別在意，就這麼開始吧？」
「不，可是」
「無視不就好了麼。事態很明顯沒有進展，就這樣解散也沒關係吧」

騎士們的視線都集中在了阿謝拉和拉萊拉萊身上。
兩人水火不容，是他們的共同認知。
就在幾天前她們大吵了一架，還那麼激烈的。

「即使如此，這種轉變速度，就連我也不能隱藏住困惑了。喂，你們兩個，在聽我說話嗎？」

納基爾向她們說話，也沒有一點回覆。
阿謝拉和拉萊拉萊互相挽著手臂，互相凝視著創造出了只有兩個人的世界。

⋯⋯卻好像聽到了什麼聲音，阿謝拉看著納基爾那一邊說道。

「剛才，難道是在說我和 拉拉 的事情嗎？」
「不應該是這樣子的吧！這種距離感⋯⋯還有，那種稱呼方式！」
「拉拉就是拉拉啊，是這孩子拜託我叫她拉拉的」
「記得母親就是這樣稱呼我的，所以希望媽媽也能這樣叫我，這樣一回事」
「⋯⋯媽媽？」
「是的，阿謝拉媽媽！」

拉萊拉萊的笑容，簡直就像是孩子一樣的純粹。
面對著這不知污穢的表情，納基爾突然崩潰了。

「沒事吧，納基爾」
「瑪蒂斯，給我來點藥吧⋯⋯強效的那種，能夠逃避現實的玩意兒」
「也不是沒有，不過，會變得不能再回到現實哦？」
「那也無所謂啊！那個拉萊拉萊居然稱呼阿謝拉為『媽媽』！？這是惡夢，肯定是惡夢！話說塞因茨和大衛，你們是不是冷靜過頭了！？」

看到這難以接受的景象，領悟了辱罵兩人世界是徒勞的納基爾，將憤怒的矛頭轉到了其他方向。
但無論是塞因茨還是大衛，都只是隨聲附和著，「哈哈」

在這時，互相凝視著的兩人的氣氛高漲了起來。

「媽媽，我已經忍耐不住了」
「可以嗎？大家都會看到哦」
「沒關係，不管是誰在看著，說了什麼，我的耳朵都只能聽到媽媽的話」
「拉拉⋯⋯」

然後，她們的嘴唇漸漸靠近著。
沒有興趣的瑪蒂斯，抱頭的莉莉，發怒的納基爾，還有沒什麼反應的兩具屍體。
在這種情況下，沒有人能阻止阿謝拉和拉萊拉萊的暴走。

「én，Fú⋯⋯Múu⋯⋯Hà Mù，én Fúu⋯⋯Qiù，én á⋯⋯à，Fúu⋯⋯èn」

終於，兩人開始互相纏繞著舌頭。
僅僅這樣還不滿足，直到身體緊貼在一起為止緊緊的互相擁抱，用手掌撫摸著臀部和大腿，互相提高著。
這下子就連納基爾都無法忍耐了

「啊啊啊啊啊啊啊啊！你們到底在幹什麼啊啊啊啊！？」

雖然怒吼出來的聲音大到嘶啞，但是完全無法傳達到那邊世界裡的她們。
瑪蒂斯像在同情一樣地拍了一下納基爾的肩膀，對莉莉說道。

「團長，我只想快點下結論。国王怎麼樣了？」
「⋯⋯沒有任何辦法，国王還是一樣閉門不出。今天是為了向大家道歉才聚集起來的」
「那麼再繼續下去也只是浪費時間，解散了吧」
「啊⋯⋯真頭暈。小團長，直到結果出來為止都不要再聚集我們了，另外道歉謝罪什麼的也不想聽！」
「⋯⋯抱歉」

結果，莉莉沒有像她所想的那樣表達出謝罪，就結束了會議。
靠近門的大衛和塞因茨，最先從房間裡出去了。
在他們身後的納基爾，慢慢地按著鼻子，瞪著塞恩茨的背後。

「喂，你有洗過澡嗎？一股爛肉一樣的氣味」
「在說我嗎？可真想不到啊，讓獻給神的身體保持乾淨是理所當然的義務，這不正是我所必不可少的嗎！」
「但臭就是臭！洗澡也不能除去臭味的話，就噴點香水啊」
「如果需要的話，我也可以做給你哦？」
「嗚⋯⋯既然說到這種地步了，那我就考慮考慮吧」

四人進行著那樣的對話，離開了房間。
結束了慢慢接吻的阿謝拉和拉萊拉萊，也調著情退出了房間 ──

「莉莉」
「怎麼了，姫莉」
「之後想和你說說話，要去你的房間」
「我明白了，等著你」

一直在莉莉身旁站到最後的姫莉，留下這樣的話後也離開了。
一個人獨處後，莉莉大口吐著氣，像是把身體扔出去一樣坐在了椅子上。

是從什麼時候開始的，這孤獨的空間竟會如此的讓人安心？
回想起來，已經很長一段時間  ── 沒有體會過和誰在一起安心度過的感覺了。

也就是說，對於她而言，真正能稱得上是朋友的，就只有卡米拉。
從那之後，和千草交談了幾次。

也許是因為這個，最近經常想起卡米拉。
雖然還沒說出最後發生了什麼，但不久後就會說出來了吧。

真是不可思議。
本來打算把這些事情一起帶進墳墓裡的，可是在千草面前卻自然而然地說漏了嘴。

然後，對於感覺到了「救贖」的自己，她驚呆了。

「雖然嘴上說是自己不好，可是就結果而言，只不過是不想承擔責任而已啊，我」

無論是誰都是那樣的吧。
假設，即使百分之百的錯誤都是自己造成的，也會想要放棄承擔責任。
因為這個，與其讓剩下的人生全都變得不幸，暫且不談受害者的心情，如果只是考慮了自己的利益，那逃跑就是最聰明的選擇。

正義感不會產生任何東西。
他人的幸福是填不飽肚子的。
即便如此，如果有人說出這些話，那麼毫無疑問這個人就是異常的。

「但是我必須成為那個。我之所以在此的理由，就只是所有的那個大義名分，僅此而已」

作為騎士團長，就應該總是正確的。
莉莉從小就被父母反覆這樣教導著，不僅僅是父母，周圍的人也都是這樣期待著。

卡米拉是唯一的例外。
所以一直努力著 ── 莫奈何，她太異常了。

才能，能力，性格，決定了靈魂會不會變得異常。

如果過著普通的生活，就能普通的讓別人幸福，也能普通的讓自己變得幸福吧。
只有著那樣的能力。
但是，正因為沒有受到父母、環境、友人的恩惠，才背負了不必要的不幸。
騎士團長這樣的地位，對於莉莉而言，本來是不必要的。
因為父母希望她這樣，所以現在她才會在這裡。
那麼她的願望，又到底會在哪裡呢？

「儘管如此，我必須繼續說，自己沒有錯」

── 即便是，把最希望與自己相見的，最愛的友人殺掉。

「莉莉，又是這副表情嗎」
「⋯⋯公主」

又是。
回過神來，塞拉已在房間門前，看著莉莉。

「我，覺得那樣不好。像這樣把自己想說的話都塞在自己的心裡，總有一天會壊掉的」
「我可沒那麼軟弱，我可是騎士團長」
「說謊。而且我不是和騎士團長，而是和莉莉這個人說話」

塞拉並不把她當作為騎士團長，而是作為莉莉這個人來看待。
她知道這個。
如果莉莉是作為莉莉的話，那一定就會感到「開心」，甚至只要拜託一下，就能立刻成為友人。
但是兩人無法成為友人，最終只是騎士團長和公主之間的關係而已。
在那個身體裡流動著的血脈，阻礙著，兩人的接近 ── 不，正確來說，阻礙著莉莉內心的解放。
只要塞拉是克里亞萊茲家的一員，莉莉就一定不會停止作為騎士的言行舉止吧。

「這是命令，莉莉，請不要把我當成公主」
「又胡來了」
「即便公主的胡來命令，也要遵從，才可謂是騎士團長！」
「這實在太胡來了，公主」
「就算是胡來，如果不這麼做的話，就無法拯救莉莉！」

儘管如此，塞拉還是沒有停止對她說話。
不想放棄。
從小開始，塞拉就很孤獨。
父親不是父親，只是国王，母親對繼承了父親血統的她也並不友好。
雖然周圍有很多人，但是很孤獨。
那之中，唯一看著塞拉的眼睛說話的人，就是莉莉。
兩人的相遇，是在塞拉開始懂事的時候，但是對於人格的形成還是有著很大的影響。
莉莉可能並沒有察覺到，塞拉從小開始就真心的，與公主與騎士的立場無關的，喜愛著她。
雖然塞拉出生於那麼腐爛的父母那裡，但能夠這麼好好地成長起來，正是多虧了這一點。

「我喜歡莉莉，但卻被莉莉以對待公主的方式來對待。你明白，這是多麼悲傷的事情嗎？」
「⋯⋯非常，抱歉」
「並不是想要你道歉。莉莉，你的職責，不應該是讓我變得不幸！」
「非常抱歉，公主」
「莉莉！」

莉莉橫著通過，從房間裡走了出去。
在她的背後，塞拉拚命地呼喚著。

「我不會放棄的。絶對，絶對 ── 要讓莉莉幸福！」

感情動搖著，胸口變得難受。
其實是，高興得想哭。
但是，即便如此，莉莉的表情依然沒有變化。

『你無論什麼時候都要繼續當騎士團長』

在她的腦海中，父親那低沉的聲音在不斷地重複著。


◇◇◇

「可我還是無法相信，無論如何，那個阿謝拉和拉萊拉萊，即便是世界被顛覆了也不會變成那樣」

會議結束之後，納基爾為了讓心情平靜下來，在城堡中來回走動著。
一個人待在房間時，就會想起剛才那令人震驚的情景，無法冷靜下來。

「衝擊性太強啦，這是醜聞，話說塞恩茨和大衛，那反應太淡定了！還有瑪蒂斯，第一次看見他這麼正經對待小團長！」

在那以前，明明是對情愛毫無興趣的那些人 ── 是這種看法到此為止了嗎。
可是，度過了各種各樣生活的納基爾，還是無法接受。
一副慌慌張張的樣子走來走去，偶然間路過了阿謝拉的房間。
於是，納基爾在離房間稍微遠一點的地方停了下來，開始靜靜地觀察著門。

「在門外側耳傾聽，就能聽到那個聲音吧」

那不是性方面上的興趣，只是單純的好奇心。
以靈魂上講不相匹配的兩人，到底是如何進行情事的。
就像是觀察著新發現生物一樣的心情。

「不，不是在偷聽，只是為了滿足求知的好奇心。我沒有錯，錯的是在那種場所下大白天堂堂正正做那種事情的兩人。是的，就是那樣的！」

這樣向著不存在的某人說著藉口，抹殺腳步聲，慢慢地靠近了門。
他在那裡注意到了。
門居然是微微開著的。
而且在接近時，就開始聽到了裡面的聲音。

「én á⋯⋯áa⋯⋯媽媽啊，那裡，áaaa én！好舒服，好舒服」
「真是好孩子拉拉，再多叫一些」
「Yá á úu én！á ū，Há，Yá áaa！」

納基爾不由得差點發出聲來，慌忙用手捂住了嘴角。
難道，真的在做。
就這樣悄悄地靠近縫隙，用一隻眼睛窺視著房間的裡面。

「啊，這不是穿著衣服嗎。只是把手伸進嘴裡而已⋯⋯可是為什麼拉萊拉萊，會那樣舒服得喘著息」

是的，這兩人絶對，不是赤裸相擁著。
阿謝拉把食指和中指放入了拉萊拉萊的口中，她拚命地含著那手指。
納基爾凝視著，舌頭上似乎刻著心形紋身。

「在舌頭上刺青，又是讓人恐懼的事情。聽說那樣子很疼」

一個人喃喃自語時，阿謝拉從嘴裡拔出了手指。
然後強行把拉萊拉萊的臉轉向了自己，像進食一樣重疊嘴唇。

「Fú úuu，én，én Gó，óo⋯⋯èn，én én én én！」

用舌頭單方面地蹂躪，暴力性的接吻。
阿謝拉像是得了虐待性變態症似的微笑得，看著拉萊拉萊的顫抖著的身體。
那種樣子，怎麼看都不普通。

腳趾用力豎直，大腿以內八字型蠕動著。可是，卻用手臂緊緊抱住了阿謝拉的後背。
就好像兩人真的在做愛一樣。

一分開嘴，拉萊拉萊就以完全溶化了的表情看著阿謝拉，像撒嬌一樣地小聲說道，「媽媽」

於是，阿謝拉把拉萊拉萊抱在胸前，就像是母親對待孩子一樣，溫柔地撫摸著她的頭。

「那開始吧，拉拉」
「嗯，媽媽」

那句話，提高了納基爾的期待。
果然，目前為止都還只是前戲。

「終於能看到兩人做愛了⋯⋯！」

納基爾像是１５歳左右的男子一樣開心著。
但是辜負這樣的期待，兩人開始了意料之外的行動。
以為會再次相擁時，阿謝拉把臉埋入了拉萊拉萊的脖子上。
然後張開嘴，咬了下去。

「én á áaaa á á á！」

接著，拉萊拉萊仰面朝天，發出了巨大的聲音。
此後，半張著嘴巴，流下口水，顫抖著聲音。

「áa，á⋯⋯èn àa，媽媽，進來了⋯⋯」

納基爾一開始並不知道她們在做什麼。
但是，他發現阿謝拉的喉嚨在動著。

「難道這是在，吸血？騙人的吧，不過，那個阿謝拉確實像是會喝血的樣子，即便是那樣，這也是極其特殊的性癖」

像這樣，半開玩笑地嘟噥著。
當然，這時他已經注意到這個行動不是單純的特殊性癖。
繼續這樣觀察下去是因為，需要確實的證據。
不久後，被持續吸血的拉萊拉萊，變化著肌膚的顏色，肉體也變得更加色情。

「⋯⋯小團長，雖然我不相信你，不過，這下真的不妙了」

納基爾知道卡米拉的事情。
快要魅惑了公主的，被莉莉消滅的吸血鬼。

可是 ── 那個名義騎士團長，真的能做到那種事情嗎？騎士團裡的人對此持懷疑態度。
不過，這實際上不是卡米拉，而是更危險的吸血鬼幹的。
這時，納基爾完全沒有理由會知道這個。

「á⋯⋯Fú⋯⋯Fú Fú，Fú Fú Fú⋯⋯媽媽。這樣子我，就變得，和媽媽一樣了」
「歡迎拉拉。這樣，我就能和拉拉永遠地結合在一起了」
「啊，媽媽啊⋯⋯」

房間裡，已經淪為了吸血鬼的拉萊拉萊和阿謝拉，為慶祝誕生日而開始了親吻。
然後同時脫下了衣服。
這次才要把身體重疊起來吧。

「那麼，怎麼辦呢？首先，先去問問退治魔物的專家吧」

納基爾沒有興趣看怪物的交合。
他先與房間保持了一定的距離，再直接朝著塞因茨房間的方向前進。

嘭嘭。

敲了門。沒有回答。

「難道因為，在意剛才的事情，而在淋浴」

又或者是，去了其他什麼地方 ── 試著將手放在門把手上，門沒有上鎖。

「啊啦，真粗心啊」

開門的同時，從房間裡溢出來了讓人不快的味道。
和剛才塞因茨身上漂浮的相比起來更加惡臭的氣味，納基爾反射性地皺起了臉。

「那傢伙，到底在做什麼⋯⋯！」

如果可以，現在就想立刻離開，但不可以那麼做。
納基爾踢開了門。
然後，他進入房間，發現了躺在床上的塞因茨。

「哦呀，這不是納基爾嗎。怎麼了？」

他好像什麼事都沒發生過一樣，普通地說著話。
但是，他的右手臂上沒有肉。
胸上的肋骨也暴露了出來，像腐爛了的肉一樣的顏色的心臟，咚咚地令人毛骨悚然地脈動著，吐出著血液。
總之，剛才開會時，散發出來的臭味就是他自己的腐臭味。

「你，這⋯⋯」」
「哎呀，真是糟了。其實我，在來這座城堡之前就已經死了，至今為止都是為了實現某個人的目而假裝活著」
「什麼，這到底是為什麼會變成這樣！？」
「真是不可思議啊。我擅長光之魔法，據說這是與之相對的影子的力量。但是，屍體的腐爛是無法停止的，既然已經臭到會被納基爾指出來的程度了，那就已經不能完成任務了」
「不明白⋯⋯完全不明白啊」
「我也不太明白。但是，這樣子在你面前暴露了真實姿態，也就是說 ──」

塞恩茨露出了沒有陰影的笑容，說道。

「使用完了，這麼一回事吧」

然後心臟像氣球一樣砰地一聲炸開了，再也一動不動了。
被留下來的納基爾，張著嘴呆呆地站在那裡。

「這是，死了嗎？這種⋯⋯這種事，就算是魔法，也不可能做得到！」

粗暴地關上了門，向著另外的房間前進。
他也是騎士，並不會被這種程度的冒牌貨所迷惑。
納基爾接下來前往的是大衛的房間。
不敲門就直接握了門把，果然沒有上鎖。
但是大衛平時就很粗心，經常不鎖門。
所以沒有特別在意，一口氣打開了門。

「大衛，在嗎！？」

大衛，少見地坐在了椅子，面對著桌子，好像在做著辦公事務。

「有重要的事要說」
「嗯，怎麼了納基爾」

然後，他不轉動身體，只把腦袋部份旋轉一百八十度朝向了納基爾。
這是人類所做不出的動作。
就算那是遠在人類之上的大衛。

「⋯⋯你不會，也」
「啊是的，已經死了，我應該已經死了。但是為什麼會在這裡呢？啊，不明白。完全不明白啊」

不太像是他的，用著泄氣的聲音說著話，開始轉動起了脖子。
轉了一圈，轉了兩圈，轉了三圈。
人類的肉體不可能做得出這種愚蠢的動作。
不久後，持續旋轉的大衛脖子被扭斷，腦袋啪嗒一聲掉在了地板上。

「啊，啊，不明白。這是怎麼了，我不明白，我怎麼了，完全不明白。納基爾，我 ──」

像是發條裝置壊掉了一樣，恰好，停止了動作。
脖子像噴泉一樣溢出血液，浸濕了地面。
當然，掉在地板上的大衛腦袋也被血液弄髒了一點點。

「這是，幻覺⋯⋯！這混蛋！到底是從什麼時候開始對我施加這種魔法的！？開會時？還是更早之前！？那麼，也就是說，剛才的阿謝拉和拉萊拉萊都是幻覺，幕後犯人現在肯定在笑著，看我慌張的樣子！喂，喂，是這樣的吧！？」

叫喊著，回過頭來。
以為那裡沒有人。
對自己施加了幻覺魔法的罪魁禍首，難道還會在自己眼前露面嗎。
但是，那裡站著一個，長著橙色短髮的少女。

「為什麼⋯⋯還站在那裡。為什麼要露面？喂，是你給我看這個的吧！？」

少女一動不動。

「快說話啊，喂！」

少女還是一動不動。

「我叫你說話啊，你這混蛋啊啊啊啊啊啊啊！」

納基爾憤怒地扭曲了臉，想要抓住她衣服的前襟，但她還是不動。
是的，他的手輕輕地穿過了少女，貫穿過去了。
然後少女的身體一下子溶解成了黑色影子，接著出現在了納基爾的背後。
感覺到了氣息的他，慌張地回頭一看。

「你是納基爾吧。不太想記住男性的名字啊」
「你是，誰」
「艾莉絲。為殺你而來的半吸血鬼」
「啊啦，你以為我是誰啊？是騎士哦，王国最強的騎士哦！？一個廢物程度的魔物居然說要殺我？」
「被認為是廢物了啊」
「當然，因為你，只會給我看幻覺。想用這種低劣魔法來殺我，還早了一百萬年啊！」

大聲喊叫著，把手放在腰上，握著手柄一口氣拉了出來。
那是 ── 納基爾最擅長的武器 ── 用金屬制成的強韌鞭子。
一般的鞭子是用來敲打的，但他卻不一樣。
是為了粉碎，切裂而使用的。
但是，這對於從千草那裡得到了力量的艾莉絲而已根本無所謂。

「不是幻覺，全部都是現實」
「怎麼可能，賽因茨和大衛絶不是你們這種人就能殺得掉的！」

納基爾為了切開艾莉絲的肉體，高高舉起了鞭子。
當他準備竭盡全力開始攻擊的時候 ── 手臂已經不存在了。
沒有被砍掉的實感，手臂消失了。
艾莉絲蔑笑著，困惑的他。

「做得到哦，用了姐姐大人力量的話」
「你，這 ──」

接下來是左手。
在那之後是右腳，左腳。
就像被黑暗吞噬了一樣，納基爾的部件一個接一個地消失。
然後就連軀幹也被吞噬後，他的頭一下子掉到了地板上。

「嗚，騙人⋯⋯這，是惡夢。這不可能 ──」
「已經抓住訣竅了，反正姐姐大人也說過很簡單就能恢復原狀，你這傢伙也很噁心，還是趕快最後一擊吧」
「等一下，住手，拜託了！我為了變得更加美麗，還有想做的事情！」
「男性怎麼可能會變得美麗，你是傻瓜嗎？」

艾莉絲助跑了一下，把滾落在地上的球踢飛了起來。
咚啪！
飛翔著與走廊裡的牆壁猛烈碰撞，像被割開了的水氣球一樣碎裂了。
周圍飛散著血液和類似他頭部碎片的東西，但是很難判斷出是不是納基爾的一部分。

「艾莉絲，做得可真華麗啊」

一看到出現在那裡的千草，艾莉絲的表情一下子就變得開朗了起來。

「啊，姐姐大人！誒嘿嘿，其實從輸給蕾雅那時候開始，就只有一次也好，想試著打倒強大的人類。比起那個，你看，我努力了，表揚我吧」
「我知道了，你看」

千草張開雙手，擁抱住艾莉絲。
看著被撫摸著頭，臉頰貼著自己胸口，高興的她，千草也微笑了起來。

「嗯！被姐姐大人觸摸的時候果然是最幸福的⋯⋯啊，這麼說來姐姐大人，下一個目標好像是姫莉吧」
「嗯，已經給阿謝拉和拉萊拉萊下達指示了。不久就會完成命令了吧」

正如千草所說的，被賦予了命令和力量的阿謝拉和拉萊拉萊已經為了魅惑姫莉，而開始行動了。
在那之後，就只需要讓塞拉和莉莉墮落了。

不過在那之前 ── 必須獲得卡米拉的真相。
因為那才是，現在這個千草的起源。