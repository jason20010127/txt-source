﻿
在從國家那裡得到報酬，得到怪物掉落物品的第二天。
露易絲還沒有回來，正好趁今天快點去吃和露露妮約定好的美食。
......順便一提，雖然是經我之手打倒的怪物掉落品，該說果然如此嗎？
似乎平時沒什麼機會得到這種貴重的怪物掉落品，對掉落品進行分類的士兵們都驚呆了。
但是，全部都是我的功勞
──不止士兵們，就連身為國王大人的藍澤先生都稍微地這樣感覺到了，但是藍澤先生似乎認為這只是某種幸運而已。
......國王大人覺得這樣就好了嗎？
嘛啊，我能夠不用擔心什麼就這樣解決的話倒是得救了......
於是，我得到的怪物掉落品大概如下：

『千手甲』――神話級裝備品。自身發動攻擊的時候，攻擊的數值會增倍。而且，普通攻擊時會賦予光屬性。

『空王的長靴』――神話級裝備品。在空中展開立腳點，能夠在空中自由自在地行走。而且，腳上纏有旋風，攻擊的時候提升速度也成為可能。

『水神的水筒』――神話級裝備品。再怎麼喝也不會減少的水筒。喝了裡面的水的話能夠解除所有異常狀態，使對象的身體朝健康體改變。

我好像又殺了神的樣子。
又是一場能力值大飛升的大遊行啊。

『千手甲』的效果是，例如向某只怪物一次性釋放100次斬擊，效果加成之後就會變成200次斬擊了。
而且，還被賦予了光屬性的狀態......這個怪物肯定不是我對吧？
得到報酬之前都沒有在意，整理從打倒的魔物那裡入手的情報發現，是名為『千‧魔像』的魔物的知識，我覺得是從那傢夥身上得到的資料。

『空王的長靴』是莎莉婭穿著的『蒼之靴』的升級版。
不但沒有空中行走的限制，而且還擁有能夠提升速度的破壞平衡的性能。
這裝備實在是搞不清楚是從哪隻魔物身上得到的。
畢竟，『飛龍』、『天獅子』之類的擁有很像大王的名字的魔物實在是太多了。
雖然和防具不太一樣，但是『水神的水筒』也有著作弊一般的性能。
雖然可能和我沒什麼關係，但是竟然能解除所有的異常狀態？
而且，筒中的水並不會減少，還能夠改變成健康體質，連病都能夠治好？
這也太犯規了吧。
這個掉落物也是不知道從哪隻魔物身上得到的，『水蛇』、『水君』，這之類的也太多了。
雖然還有很多從打倒的魔物的各個部位得來的其它的裝備品，但是一一講述也太費時間了。
如之前所說，今天計畫和露露妮一起尋遍美味食物，所以打算一大早就翹掉早飯事先去路攤。
莎莉婭她們有的接受工會的委託，有的則打算在房間度過。
這個先放在一邊，讓露露妮等待就不好了，立刻準備好之後我就離開房間了。

◇◇◇◆◆◆◇◇◇

在客棧外等了一會兒露露妮就來了。

「啊，主人，不好意思！那個......是不是讓您久等了？」
「沒、沒有這回事喲。比起這個，今天不是要去吃美食嗎？早上還沒吃，現在肚子已經餓扁了啊......。」
「是，是這樣啊！那麼，現在就出發吧！」

露露妮一臉笑容打算就這樣立刻出發。
雖然肚子確實是空空的，但是今天打算好好地嘗遍美食，所以我抓住露露妮的手止住了她。

「露露妮，就算不這麼急著去食物也不會逃走的喲。慢慢地走就行了好嗎？」
「啊......說，說的是呢......所以那個，主人......。」
「嗯？」
「手......。」
「啊，抱歉！」

露露妮害羞地這樣說的瞬間我才發現依舊抓著露露妮的手。
雖然慌慌張張地想要分開手，但是露露妮卻反過來握住了我的手。

「露露妮？」

感到驚訝的我看向露露妮，但是露露妮卻保持低著頭的狀態說。

「......今天一天就可以了......就這樣一直牽著手可以嗎......？」
「......。」

這孩子是誰啊。
雖然看慣了最喜歡食物，分不清是凜凜還是天然的露露妮，但是耳朵赤紅，害羞著的露露妮至今為止都不曾見過。
對與平時完全不同的氣氛感到困惑的時候不經意間愣住了，而露露妮則不安地向我搭話。

「主人......？」
「啊，沒，沒事！那個，如果我這樣的可以的話，想握多久都可以！」

怎麼回答的啊，我。
惰性的我只能做出微妙的回應。
但是，露露妮雖然因為這句話臉頰稍微變得赤紅，依舊開心地笑著。
對於突然的笑容，這回換我的臉變得赤紅了，仿彿沒有意識到的樣子，我錯開臉龐，開始移動了。

「那，那個！一直在同一個地方的話也很困擾吧？適當地逛一下路攤吧。」
「嗯！」

雖然感覺在出發之前就花費了不少時間，但是我們終於出發了。
沒有特定的目的地，像這樣閒逛著也挺不錯的，這次的路攤數量也很多，直到抵達廣場前都在逛著。
考慮著接下來要不要帶露露妮一起去諾亞德先生的茶館，畢竟那裡的糕點和紅茶都很美味啊。

「啊，主人，請看那個！」
「嗯？」

在走向廣場的途中，露露妮突然止住腳步，指向某個地方。
從那個方向看過去的話......

「......大胃王大賽？」

巨大的板子上告示著大胃王大賽開幕中。
眼前的食堂好像正巧舉行大胃王大賽的樣子。
看向食堂的名字，發現上面寫著『滿腹食堂』。
......嗯，就算是舉行大胃王大賽也不會感到奇怪的店名。
本來想在路攤吃飯的，但是這種的也很有趣。
而且繼續看下去的話就會發現板子上寫著優勝者可以免除在大胃王大賽中所吃料理費用的獎賞。

「露露妮，要不要參加看看？」
「哎？但是......可以嗎？」
「噢嗚，我之後再吃就可以了。我覺得露露妮是想要吃的吧，我也會跟著去的。」
「是這樣啊......那麼......。」

露露妮在一瞬間的迷惑之後就決定要去參加了。
於是，食堂的店員向露露妮傳達參加的規則......

「那個......真的可以嗎？這次大賽是世界中有名的大胃王們齊聚的戰鬥。沒有女性的專屬部門，只能混在男性中一起比賽......。」

有名的大胃王家是什麼？
而且還是在世界中這種規模啊。
店員姑且這樣忠告了，但是露露妮毫不畏懼......

「沒問題！我要吃啊！」

意識已經被食物吸引過去了，是平常的那個露露妮啊。
店員雖然被露露妮的熱血所震撼，但還是按照職業素養認可露露妮的參賽資格。
進入食堂之後，確實能夠感覺到屬於大眾食堂的氣氛，許多巨大的圓形桌子並排著，其它的還有和店主專屬的櫃檯席相似的觀眾席，連陽臺席位都有。
露露妮走向其他參賽者那裡，而我則坐在能夠清楚地看到露露妮的觀眾席。
於是，忽然從旁邊飄來煙霧。
下意識地看向那個地方，有一個被破爛的黑色斗篷包裹住的人坐在旁邊的座位上。

「哎呀，抱歉啊！這煙，不管怎樣都戒不掉啊......。」

旁邊的人因為風帽的關係沒辦法看清整張臉，嘴上叼著香煙，嘴邊滿是鬍子茬，從聲音也能夠判斷出是男性。
......話說，這個世界也有香煙啊......

「啊，不，沒關係的。」
「是嗎？這還真是謝謝了啊......大叔，給我來一瓶淡色啤酒！對了，你要點什麼？」
「哎！？」
「作為煙的賠禮，要喝什麼？」
「怎麼可以！並不是什麼大不了的事！」
「好了，讓我請客就行了！所以呢？要什麼好？」
「啊......那個，這樣的話，來點果汁......。」
「知道了，大叔，追加美味果汁一杯。」
「知道了！」

記得淡色啤酒好像也是啤酒的一種？
不知道有沒有加入啤酒花進行釀造，在地球的時候曾經在電視上看到過......
在我思考著這種無所謂的事情的時候，旁邊的男性拿走淡色啤酒，把橙汁放在我的面前。
那名男性一口氣把啤酒幹完了。

「卡啊啊～～！真美味啊！」
「嗯，確實很好喝。」

我的不是酒而是果汁，似乎是百分百的果汁，那種濃厚是由果實原本的美味凝縮而成的。
喝得非常暢快的男性忽然向我搭話。

「說起來......這裡還真是一個不錯的國家啊......你不這樣覺得嗎？」
「哎？啊，是，說的也是。雖然我不是這裡出身的，但還是覺得是一個不錯的國家！」
「說的對啊......我啊，經常出差，就算是這如同村莊般的小國家也有好好地管理著啊。管理這個詞好像讓人感覺不太舒服啊，並不是壞的意思哦？能夠好好地看清整個國家，騎士團也經常確保周邊的安全，村子（國家）陷入稅務糾紛的話，馬上就能夠實施對策和相應的救援......公共事業也做得很好，貧困層才會減少，對於那些極度貧困的階層也能夠好好地對待，簡直就是理想般的國家啊？」
「是這樣啊......。」
「這裡的國王超厲害的喲。去別的國家的話就會發現，那種理所當然般的人種差別對待，沒有能夠安心休息的地方......對了，你是冒險者嗎？」
「那個，是的！」
「這樣的話，總有一天會離開這個國家到其他國家工作，那時候，不要以這個國家作為基準來考慮哪。這裡在充滿和平和尊重的同時，也存在著危機感薄弱的弊端啊。」

或許是吧......工會方面也還是不要以這條街上的工會本部作為基準吧。
話說，其他工會也像這裡一樣滿是變態的話也很困擾啊。
變態只有這條街就夠了。

「說得是，謝謝你！」
「不用啦！說起來，還沒有問你的名字啊。」
「啊，我叫誠一。」
「誠一嗎？真是個好名字啊。聽起來真響亮，是東方的國家來的嗎？我是......思勞。嘛，在這裡相遇也是某種緣分，如果能夠在哪裡再次相遇就好了啊......大叔！錢，就放在這裡了哦。」

只說著這樣的話，男性......思勞先生一邊被煙纏繞著，一邊走出食堂。

「感覺，真是個不可思議的人啊......。」

和不知不覺變得友好的思勞先生交談的時候，大胃王大賽的準備似乎完成了，聽到主持人的聲音了。

『那麼，終於要開始了！這個『滿腹食堂』的名產大胃王大賽！接下來就介紹參賽選手！首先是這條街的第一大胃王！『度子‧俄了』選手！』

「嗚喔喔喔喔！我才是真正的大胃王之主！」

第一個介紹的選手雖然不到伽魯斯那種程度，但還是擁有相當的肌肉量的穿著背心的男性。
......話說，大胃王之主是什麼鬼？
而且，名字......

『接下來的是來自凱澤魯帝國的大胃王！『蘇夏克』選手！』

「吾輩才是真正的大胃王之主！」

接下來介紹的是長著凱撒胡，被類似軍裝的衣物所包裹的半老的男性。
......這回的名字是『咀嚼』來著的嗎？

『依舊是來自他國的挑戰者！巴爾夏帝國的美食家大胃王！『優庫塔貝爾』選手！』 （好く食べる）
「呼呼呼......能贏過我的胃袋嗎？」

無謂地笑著的中年男性，漂亮地整理好的鬍鬚，看起來就覺得很能吃的身體被豪華的衣服所包裹住。
......已經連吐槽的力氣都沒有了，『很能吃』就如同字面上的意思吧？
比起這個，我還是第一次聽到這個國名啊。
凱澤魯帝國是和我有著深切關係的地方，剛才和思勞先生談話的時候覺得是理所當然，但是現在再次深深體會到除了這個國家以外還有其他很多國家這件事。

『最後是！這次大會的唯一一名女性！挑戰者『露露妮』選手！』
「前置就算了，快點開吃吧！」

露露妮還真是無禮啊。
完全感覺不到緊張的露露妮，對此感到無奈和欽佩的主持人繼續進行大賽的說明。

『那麼，接下來請允許我說明一下本次大賽的規則。規則就是，按照順序吃完這邊事先準備好的料理，最後留下來的人就是勝利者。當然，料理的量和種類，順序都一樣！優勝者不僅能夠免除這次消費的費用，還能夠得到我們店主得意的作為甜點的一品『巨大聖代』！』

哦哦，沒有寫在板子上，能夠得到甜點啊。

『那麼，接下來是第一道料理！料理名是......『馬鹿的烤肉』！』

意想不到的馬鹿！？
話說，露露妮吃了的話不就是同類相殘了嗎？
在驚愕的我對面，在選手的面前，美味的烤肉仿彿要滿溢出來般並排著。

『那麼，各位都準備好了嗎？那麼......開始！』

配合主持人信號，選手們一齊啃起了烤肉。
露露妮也和其他選手一樣，吃起了烤肉。
啊啊......同類相食......
烤肉漸漸地被選手們收入胃袋中，度子‧俄了選手的進食速度漸漸下降了。
於是──

「嘛，受不了了......！嗚卟！」

度子‧俄了選手落馬了。

喂！？這不還只是第一品而已嗎！？
確實就算是外人看來也是非常驚人的量的烤肉，但是至少給我吃完第一枚啊喂！
度子‧俄了選手連烤肉的一半都沒吃完，讓人不禁好奇他為何要來參加這種比賽。

『哎呀，這麼快就有一人落馬了！那麼，度子‧俄了選手這次的料理還有剩，包括殘飯在內，要連剩下的部分一起支付喲！』

主持人這樣說著的時候，一個店員走進度子‧俄了選手，把一枚紙遞給他。
恐怕是帳單吧。

「！？」

把帳單接過來的度子‧俄了選手一副蒙克呐喊般的表情。
......上面到底寫著怎樣的金額，反而讓人在意了。

『那麼，其他的選手們好像都吃完了！那麼接下來，鏘鏘地繼續進行吧！』

於是，接連端上來的料理都漸漸被選手們消化了。
......度子‧俄了選手，太對不起這名字了吧？
第二品，第三品......
接連不斷地繼續進行著，終於第二個落馬者出現了。

「已經......不行了......！」

『嗨──咿！蘇夏克選手，落馬了！那麼，請支付這裡寫著的金額！』

按著肚子，看起來似乎非常痛苦的蘇夏克選手的旁邊，店員再次現身，把帳單遞給他。

「！！？？」

於是，第二人的蒙克呐喊也出現了。
所以說，到底寫著怎麼樣的金額啊？

『看，又有一個落馬了！沒想到，露露妮選手竟然能夠存活到這裡，實在是無法想像啊！』

如主持人所說，一般來說，像露露妮這樣的美少女說是大胃王也不會有人信啊。
但是，在近處看著的我，比起懷疑，我反而更好奇那身體是如何收納如此大量的食物，儘管感到不可思議卻無從認證。

『那麼，在一人落馬之後，這邊也只剩下最後一道料理了。但是，和至今為止的料理不同的一線料理已經準備好了。那就是......請看這邊！』

主持人這樣說的瞬間，店員把體長5米長的巨大烤全鳥並排擺在露露妮和優庫塔貝爾的面前。

『最後的料理就是，『沼澤鳥的烤全鳥』！真是很符合最後一戰的一道料理啊。』

看到並排放在眼前的料理之後二人的反應截然相反。

「庫......！沒想到竟然還有重量級的料理等著我......！」
「喔喔喔！這不是看起來很美味的鳥嗎？」

露露妮高興地啃向料理，優庫塔貝爾選手雖然吃的速度很慢，但還是努力堅持著要吃完。
但是──

「～～！」

優庫塔貝爾選手按住嘴巴，直接把椅子當做床橫躺在上面。

『哎啦啦！優庫塔貝爾選手出局了！也就是說......這真是令人難以置信啊！？優勝者是......這次大賽的唯一一名女性，露露妮選手！』

「「「嗚喔喔喔喔！」」」

突然響起來的歡呼聲，我在被嚇了一跳的同時看向周圍，不知不覺中這裡擠滿了觀眾。
......太過集中了，結果沒注意到......
而優勝的露露妮本人則......

「嘸？優勝？比起這個，沒有其它的料理了嗎？」

好像還沒有吃夠的樣子。
你的胃裡面到底是什麼構造啊？
無奈地，一臉難受的優庫塔貝爾選手站了起來，為了向優勝者送上祝福，獻上掌聲以示歡迎。

「呼......呼呼呼......沒想到，我竟然會輸啊......而且還是輸給她那樣可愛的女性......。」
「因為我很喜歡吃東西哪。而且，在吃飯之前，跟雄雌沒有關係。」
「......男女平等是嗎......能不能問你一個問題。對你來說，進食意味著什麼？」

面對這哲學般的提問，露露妮不經任何思考就立刻回答了。

「生與死的調和，還有，歷史吧！」
「......這個，怎麼說？」
「進食是為了生存而奪取其他生命的行為。如果欠缺其中一部分的話，『食』這一概念就不復存在了。而且，吾等能夠像這樣品嘗許多類型的料理，也是先人留下來的對於『食』的探究的功勞。這種行為，和是雄是雌，人種還有宗教，神和人都沒有關係。『食』是神無法侵入的為數不多的聖域之一，我總是這樣想的。」

好深奧，太深奧了啊。
我沒想到露露妮竟然如此認真地思考著『進食』這一回事。
聽到露露妮的回答，優庫塔貝爾選手似乎領悟到了什麼，浮現出非常爽朗的表情。

「是這樣啊......看來我還是不夠成熟啊。離開祖國參加這次大賽，能夠觸碰到少女的真理真是太好了......。」

看著優庫塔貝爾和露露妮的爭論的觀眾們，感動地想她們送去掌聲。
於是，店員不知何時已經站在優庫塔貝爾的旁邊，把類似帳單的東西遞給他。

「！！！？？？」

第三人的蒙克呐喊出現了。
這什麼鬼。
我只能陷入發愣的狀態了。

