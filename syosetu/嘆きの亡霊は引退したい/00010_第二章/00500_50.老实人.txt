「你說你自己⋯⋯什麼都不知道？」

「嗯，運氣太差了呢。不過幸好平安無事」

我聽完事情以後坦率地說出自己的感想，對此斯維恩是抱著頭。

動蕩的一夜過後，氏族之家的族長室聚集著與『白狼之巣』的調查有關的主要成員。
成員有在那邊進行指揮的斯維恩，值得依賴的『黑金十字』的成員，其他隊伍的幾名獵人，以及葛庫和身為他左右手的凱娜小姐，最後是有點印象的帝國『遺物調查院』所屬的男性調查員。
氏族的族長室也用作是接待室，所以具備一定的寬敞度，不過聚集這麼多人，室內也會顯得狹窄。

也許是吃了不少苦頭，斯維恩他們的表情十分的疲憊。

看樣子在我們遇見奇妙的格雷姆，受到從巢穴爬出來的一幫人的魔法攻擊時，他們也被卷入棘手的事情當中。
他們的目標本來就是調查棘手的事情，所以這也是分內的工作，不過還是同情他們一下吧。

在聽取事情的過程中，他們不停地責備我，像是說我隱瞞情報之類的，可即便這樣說我也只是讓我覺得怪異。
如果真的是我隱瞞不說，那被別人發火也是正常，可實際上我的確是什麼都不知道。

望著調查員急躁地跺著腳，我在沙發上深深地嘆了口氣。

也許是因為我是八級獵人，別人總是會高估我。我也習慣被人抱怨了。
我很沒用的。身體既不怎麼有鍛鍊，記憶力也不好。在此之上，運氣還相當差。
以獵人來說，我唯一的優點就是比較溫順（而這也不是獵人當中的優點）

即便跟我講有奇妙的魔術結社做過實驗而留下的痕跡，這件事也在我的處理範圍之外。

幸好似乎沒有出現死者，所以我在意的就只有一件事。

「那只像是史萊姆的幻影沒有被絲特莉的藥水溶解對吧？」

「⋯⋯是啊。根本完全沒有半點效果。開什麼玩笑！」

斯維恩怒氣衝衝地撂下這句話。

我最清楚絲特莉的藥水效果。既然她說是必殺的藥水都沒有效果，那就代表那不是西迪史萊姆吧。
襲擊斯維恩他們的可怕『幻影』之所以會有點像是史萊姆，只是偶然而已。

可話又說回來，西迪史萊姆究竟到哪裡去了呢⋯⋯⋯

葛庫就深深地坐在隔著張桌子的正面處，一直在默默地聽取。他此時是頭冒青筋，開口說道。
與那猶如惡鬼般的緊繃神情相反，他的聲音頗為平穩。然而，這沒辦法蒙混過我，葛庫現在是十分憤怒中。

「克萊伊。你的確是──優秀的男人。我是不知道你怎麼得到這些情報的，獵人不願透露自己的底細是理所當然的事情。所以關於這件事，我不會多說什麼。但是，這次找到的那玩意，是應該交由澤布爾迪亞處理的事件」

「嗯嗯，說的也是呢⋯⋯」

那就讓對方去處理不就好了⋯⋯⋯雖然想這樣回他，不過要是我這樣說，他肯定會更加不開心吧。
如果是不會危及生命的事情，我也是想盡一臂之力的，然而我什麼都做不到。

緊接著，男性調查員──至今見過幾次面的年邁調查員雙手抱胸，以宛如看見殺父仇人的眼神瞪視著我，開口說道。

「操控瑪娜源的實驗可是十罪。特別是『幻影』相關的實驗，基於它的危險性，我國是嚴禁這種實驗的。你身為帝國的臣民，有義務配合我們的調查。克萊伊・安德里希。隱蔽情報是一種罪行。即便是八級獵人也不例外」

「⋯⋯我也沒有隱瞞就是了⋯⋯」

「⋯⋯說謊也是偽證罪。我們這邊有看穿謊話的寶具。正如你所知道的，它的結果就是絕對。無論是多麼神乎其神的欺詐師，都不可能瞞得過它」

「⋯⋯我無所謂哦，請便？」

畢竟我也是經常卷入事件當中，所以有過幾次寶具證明我清白的經驗。
我既沒有做虧心事，也沒有半點頭緒，所以即便測出我的真心也完全是不痛不癢。

望著不以為然的我，男性調查員沒有取出寶具，而是狂抓著自己的頭髮，眼皮抽動不停。

「⋯⋯⋯⋯媽的，你差不多給我適可而止了吧！你他媽的究竟是怎麼躲過寶具的判別啊！次次都是這樣──」

就算你這麼說⋯⋯⋯
每次寶具證據我的清白時，你不都是一副看見絕望噩夢的表情。明明我都沒有說謊⋯⋯⋯

我環視斯維恩以及其他人。看樣子這次沒有人站在我這邊了。無論是斯維恩還是葛庫，大家都是險峻的神情。
儘管抬起頭可以看見呆在後頭的伊娃，不過她以為難的表情對我搖搖頭。連她似乎都不會站在我這邊。

究竟是怎麼了⋯⋯明明我只是對不知道的事情說我不知道而已⋯⋯⋯

這也是緊急事件，我又不可能說謊。

當我困惑時，葛庫深深地嘆了口氣。

「已經預料到進行實驗的秘密結社──就是『虛空之塔』。聽說你們『嘆息之亡靈』正在追踪他們」

『虛空之塔』？我最近才聽說這個名字。而且還說『嘆息之亡靈』在追踪他們？
我姑且是有留意隊伍成員的⋯⋯可這件事還是第一次聽說。我歪著頭尋思一陣子，果然還是沒有印象。

「這項消息是從哪裡聽來的？」

「似乎是塔莉亞聽絲特莉說的。說是『嘆息之亡靈的敵人』。還說其中的諾特・科庫雷亞以及索菲亞這兩個名字的魔法師很棘手」

聞言，站在後方的體格嬌小的女孩子微微一抖。

我聽說過塔莉亞這個名字。雖然沒見過面，不過是絲特莉的朋友，也是我家氏族裡極少的錬金術師。由於絲特莉的性格有些孤僻，所以身為她朋友我有些擔心，不過我還記得當時聽到她交到朋友的時候自己放心多了。

我輕輕向對方揮揮手，稍微想了一陣子，向以嚴肅表情等待我發言的葛庫說道。

「對不起，我果然還是沒有印象⋯⋯⋯既然我都不知道，那大概是沒什麼大不了的敵人吧⋯⋯絲特莉也不可能什麼事都跟我說⋯⋯」

「⋯⋯媽的，做到這樣你還想給我們帶來試練嗎！」

葛庫用力砸向桌子。

總覺得過意不去，對不起，我太沒用了。

我知道我家氏族的成員會將因為我的過失而受苦的事情稱之為試練。
我不是有意的⋯⋯不是故意的啦。雖然說這樣說估計也沒有什麼說服力，其實只是運氣太差而已。

我寶具的魔力都沒有了，還能做得到什麼事啊⋯⋯⋯

沙兔研的人好像就是自稱諾特，不過我總覺得自己已經徹底搞不明白了。由於那幫人是沙兔研究員的前提說不定會瓦解，所以我決定當做沒聽說過。
做族長的還是需要一定程度的無視能力。管它是沙兔研還是魔術結社，隨你們喜歡。
歸根究底，會爬進沙兔巢穴的魔術結社，究竟是什麼鬼魔術結社，反正捕捉他們也不是獵人的工作。

「好了，到此為止」

這時，放置在牆壁的書架響起一道聲音，空出一條路。
現身的是莉茲，她伸了伸懶腰並走出來。修長細瘦的手腳上，那堅韌的肌肉令人感到一種美。
明明突然遇到這種嚴肅的場面，她也沒有半點膽怯的樣子。

莉茲眺望著聚集在這的人員以後，莞爾一笑。

「克萊伊也累了，接下來就由人家來聽你們講。畢竟這邊也發生了許多事⋯⋯可以吧？」

「⋯⋯嗯，那就全部交給莉茲了。如果有什麼事，待會再告訴我」

平時莉茲都不會聽人講話的，不過並不是說她不會這樣做。
身為獵人，莉茲在實力上各個方面都凌駕於我，知識方面也是比我還強。可以安心地交給她。

既然沒有我可以做到的事情，那還是交給她比較會有好的結果吧。

「好了，在這裡只會打擾到克萊伊的，所以人家到樓下聽你們講」

「給我等一下，莉茲──」

「什麼事？葛庫。明明人家都說要聽你講了，你還有不滿？」

面對有話要說的葛庫，莉茲是雙手抱胸，以眼神牽制他。

兩名調查員一言不發的，他們知道莉茲的暴躁性格。
然而，斯維恩對于莉茲的態度是啞口無言。

嗯嗯，也是呢。如果是平時的莉茲，現在早就發火了吧⋯⋯⋯

今天的莉茲心情是好得不得了。
連唯一缺點的暴躁脾氣都得到短暫性的改善，如今莉茲是攻守兼備，沒有半點破綻。

「⋯⋯發生什麼事了？」

「嗯⋯⋯？呵呵⋯⋯你看出來了？」

只要是知道莉茲平時的為人，任誰看見她都會納悶吧。

聽見斯維恩的疑問，莉茲露出滿臉的笑容。
她裝模作樣地撇了一眼這邊以後，雙手合十說道。

「其實呢⋯⋯⋯⋯克萊伊救了我，厲不厲害？」

「哈？」

「嘛，雖然人家也沒有生命危險，不過克萊伊擔心人家會難以應付對方⋯⋯吶，是不是很溫柔？人家都重新迷上克萊伊了。吶？」

徵求別人同意幹嘛，大家都不知道該做出什麼樣的反應。

那具格雷姆裡面似乎是有操控者，而我解放的露西亞的重力魔法將他們也卷入其中，直接打倒了。

露西亞攻擊魔法的有效範圍真令人害怕。而更讓人害怕的事情是──同樣被卷入重力魔法並迎面倒下的莉茲完全就沒有在意這件事。
即便她可以躲開格雷姆的攻擊，似乎也沒辦法避免露西亞的範圍魔法。

看見莉茲羞答答地在炫耀這種事情，我只覺得坐如針氈。

⋯⋯對不起，妨礙到你們了，對不起。

我也沒想到居然連那邊都有影響。下次我會稍微縮小範圍的，所以原諒我吧。

「那麼克萊伊，待會見」

「好的好的，待會見」

莉茲得意洋洋地向我送秋波，帶領大家離開了。
即便心情再好，似乎也沒有人可以阻止得了種族滅絕大怪獸。

留下的就只有始終沉默地聽取報告的伊娃。剛剛還覺得狹窄的族長室，如今突然覺得十分寬敞。

「⋯⋯請問這樣好嗎？」

「嗯～嘛，總會行得通的⋯⋯至今不也是這樣過來的」

我家氏族除了我以外，全都是挺能幹的，所以精神上我還是輕鬆得要死。

由於別人都不在了，我立馬用雙腳搭在桌子上，以舒服的姿勢嘆了口氣。

足跡是壓倒性的。即便那個沙兔研（假）是可怕的魔術結社，並且還組團進攻過來，這邊應該也可以應付得過吧。
我已經知道這次騷亂的原因，在露西亞回來給我充填寶具的魔力之前，我是絕對不會再做危險的事情，重要的事件也已經過去了。

這陣子我已經不想再考慮探索寶物殿的事情。
只因為一次探索，肩膀都僵硬得不行。我聳聳肩，回想起之前拜託的事情，回過頭望著伊娃。

「說起來，我之前拜託你幫我找一找帝都比較好吃的冰淇淋店，找到了沒有？」